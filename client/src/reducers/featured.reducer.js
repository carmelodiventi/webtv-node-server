import { REQUEST_FEATURED, GET_FEATURED, GET_FEATURED_FAIL, GET_FEATURED_STREAMS_CATEGORIES, GET_FEATURED_STREAMS_CATEGORIES_FAIL } from '../actions/actionTypes';


const initialState = {
	loading: false,
	items: [],
	categories: [],
	error: null
}

export default function (state = initialState, action) {

	switch (action.type) {

		case REQUEST_FEATURED:

			return {
				...state,
				loading: true
			}

		case GET_FEATURED:

			return {
				...state,
				items: action.payload,
				loading: false
			};

		case GET_FEATURED_FAIL:

			return {
				...state,
				error: action.payload.error.message,
				loading: false
			};

		case GET_FEATURED_STREAMS_CATEGORIES:

			return {
				...state,
				categories: action.payload,
				loading: false
			}
		case GET_FEATURED_STREAMS_CATEGORIES_FAIL:

			return {
				...state,
				error: action.payload.error.message,
				loading: false
			}

		default:

			return state;

	}

}