
import { AUTH_REQUEST, USER_LOG_IN_REQUEST, USER_LOG_IN_FAIL, USER_LOG_OUT, CHECK_USER_IS_AUTHENTICATED, GET_USER_INFO, GET_USER_INFO_FAIL } from '../actions/actionTypes';

let user = JSON.parse(localStorage.getItem('user'));
let server = JSON.parse(localStorage.getItem('server'));

const initialState = (user && server) ? { isAuthenticated: true, user, server, loading: false, error: null } : { isAuthenticated: false, user: [], server: [], loading: false, error: null };

export default function (state = initialState, action) {

  switch (action.type) {

    case AUTH_REQUEST:

      return {
        ...state,
        loading: true
      }

    case USER_LOG_IN_REQUEST:

      localStorage.setItem('user', JSON.stringify(action.payload.user_info));
      localStorage.setItem('server', JSON.stringify(action.payload.server_info));

      return {
        ...state,
        isAuthenticated: true,
        user: action.payload.user_info,
        server: action.payload.server_info,
        error: null,
        loading: false
      };

    case CHECK_USER_IS_AUTHENTICATED:

      return state;

    case USER_LOG_IN_FAIL:

      return {
        ...state,
        isAuthenticated: false,
        error: action.payload.error.message,
        loading: false
      };

    case GET_USER_INFO:

      return {
        ...state,
        user: action.payload.user_info,
        loading: false
      }

    case GET_USER_INFO_FAIL:

      return {
        ...state,
        error: action.payload.error.message,
        loading: false
      }

    case USER_LOG_OUT:

      localStorage.removeItem('user');
      localStorage.removeItem('server');

      return {
        ...state,
        isAuthenticated: false,
        user: [],
        server: [],
      };

    default:
      return state
  }
}