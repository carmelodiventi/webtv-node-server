import {combineReducers} from 'redux';

import FeaturedReducers from './featured.reducer';
import LiveReducers from './live.reducer'
import SeriesReducers from './series.reducer';
import MoviesReducers from './movies.reducer';
import AuthenticationReducers from './authentication.reducer';

const rootReducer = combineReducers({
	authentication: AuthenticationReducers,
	featured: FeaturedReducers,
	live: LiveReducers,
	series: SeriesReducers,
	movies: MoviesReducers
});

export default rootReducer;