import { REQUEST_SERIES, GET_SERIES_STREAMS_CATEGORIES, GET_SERIES_STREAMS_CATEGORIES_FAIL, GET_SERIE_INFO, GET_SERIE_INFO_FAIL,CLOSE_SERIE_INFO } from '../actions/actionTypes';

const initialState = {
    loading: false,
    categories : [],
    items: [],
    info : {
        info : {},
        seasons: [],
        episodes : {}
    },
    error: null
}

export default function (state = initialState, action) {

    switch (action.type) {

        case REQUEST_SERIES:

            return {
                ...state,
                loading: true
            }

        case GET_SERIES_STREAMS_CATEGORIES:

            return {
                ...state,
                categories: action.payload,
                loading: false
            }

        case GET_SERIES_STREAMS_CATEGORIES_FAIL:

            return {
                ...state,
                error: action.payload.error.message,
                loading: false
            }

        case GET_SERIE_INFO:

            return {
                ...state,
                info : action.payload,
                loading: false,
            }

        case GET_SERIE_INFO_FAIL:

            return {
                ...state,
                error: action.payload.error.message,
                loading: false
            }

        case CLOSE_SERIE_INFO : 

        return {
            ...state,
            info : {
                info : {},
                seasons: [],
                episodes : {}
            },
            loading: false
        }

        default:
            return state;

    }

}