import { REQUEST_LIVE, GET_LIVE_STREAMS_CATEGORIES, GET_LIVE_STREAMS_CATEGORIES_FAIL, GET_LIVE_STREAMS, GET_LIVE_STREAMS_FAIL } from '../actions/actionTypes';


const initialState = {
    loading: false,
    categories: [],
    items: [],
    error: null
}

export default function (state = initialState, action) {

    switch (action.type) {

        case REQUEST_LIVE :

            return {
                ...state,
                loading: true
            }

        case GET_LIVE_STREAMS_CATEGORIES :

            return {
                ...state,
                categories: action.payload,
                loading: false
            }

        case GET_LIVE_STREAMS_CATEGORIES_FAIL :

            return {
                ...state,
                error: action.payload.error.message,
                loading: false
            }

        case GET_LIVE_STREAMS:

            return {
                ...state,
                items: action.payload,
                loading: false
            }

        case GET_LIVE_STREAMS_FAIL:

            return {
                ...state,
                error: action.payload.error.message,
                loading: false
            }

        default:

            return state;

    }

}