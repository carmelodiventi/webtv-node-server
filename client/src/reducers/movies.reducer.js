import { REQUEST_VOD, GET_VOD_INFO_BY_ID, CLOSE_VOD_INFO, GET_VOD_STREAMS_CATEGORIES, GET_VOD_STREAMS_CATEGORIES_FAIL, GET_VOD_STREAMS, GET_VOD_STREAMS_FAIL } from '../actions/actionTypes';


const initialState = {
    loading: false,
    categories: [],
    items: [],
    vod_info: {
        info: [],
        movie_data: []
    },
    error: null
}

export default function (state = initialState, action) {

    switch (action.type) {

        case REQUEST_VOD:

            return {
                ...state,
                loading: true
            }

        case GET_VOD_STREAMS_CATEGORIES:

            return {
                ...state,
                categories: action.payload,
                loading: false
            }

        case GET_VOD_STREAMS_CATEGORIES_FAIL:

            return {
                ...state,
                error: action.payload.error.message,
                loading: false
            }

        case GET_VOD_STREAMS:

            return {
                ...state,
                items: action.payload,
                loading: false
            }

        case GET_VOD_STREAMS_FAIL:

            return {
                ...state,
                error: action.payload.error.message,
                loading: false
            }


        case GET_VOD_INFO_BY_ID:

            return {
                ...state,
                vod_info: action.payload,
                loading: false
            }


        case CLOSE_VOD_INFO:

            return {
                ...state,
                vod_info: {
                    info: [],
                    movie_data: []
                },
            }

        default:

            return state;

    }

}