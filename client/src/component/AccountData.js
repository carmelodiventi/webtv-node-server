import React from 'react';
import { getExpDate, hasM3U8 } from '../utils/helpers';
import FeaturedSelect from '../containers/account/FeaturedSelect';


const AccountData = ({ username, active_cons, exp_date, status, auth, allowed_output_formats }) => {


  return (
    <ul>
      {hasM3U8(allowed_output_formats) &&
        <li>
          <span>Your web tv is enabled and ready to play</span>
        </li>
      }
      {!hasM3U8(allowed_output_formats) &&
        <li>
          <span>Your web tv is not enabled ask to administrator to enable it!</span>
        </li>
      }
      {username &&
        <li><label>Username:</label> {username} </li>
      }
      {active_cons &&
        <li><label>Connected: </label> {active_cons} </li>
      }
      {getExpDate(exp_date) &&
        <li><label>Expired Date:</label> {getExpDate(exp_date)} </li>
      }
      {status &&
        <li>
          <label>Status:</label>
          {
            <span className={(status === 'Active') ? 'active' : 'expired'}>{status}</span>
          }
        </li>
      }
      {auth &&
        <li>
          <label>Auth:</label>
          {
            <span className={(auth === 1) ? 'active' : 'expired'}>{(auth === 1) ? 'Yes' : 'No'}</span>
          }
        </li>
      }
      <li>
        <label> Featured: </label>
        <FeaturedSelect />
      </li>
    </ul>
  );

}


export default AccountData;