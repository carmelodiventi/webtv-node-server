import React, { memo, useState, useEffect } from 'react';
import { genDates } from '../../utils/helpers';
import ChannelSceduleItem from './channel-schedule/ChannelSceduleItem';

const ChannelSchedule = memo(({ data, tvArchive, showing, setReplaySource }) => {

    const [activeTab, setActiveTab] = useState(0);
    const [dates, setDates] = useState([]);
   
    useEffect(() => {
        const { epg_listings } = data;
        const dates = genDates(epg_listings, tvArchive);
        const todayTab = dates.findIndex(date => date.today === true);
        setDates(dates)
        if (todayTab !== -1) setActiveTab(todayTab);
    }, [data, tvArchive])

    return (
        <div className={`channel-schedule ${showing ? 'showing' : 'hidden'}`}>
            <div className="tab">
                <ul className="tab-list">
                    {dates.map((item, idx) => {
                        return (
                            <li className={`tab-list__item ${activeTab === idx ? 'active' : ''}`} onClick={() => setActiveTab(idx)} key={item.date}>
                                {item.date}
                            </li>
                        )
                    })}
                </ul>
                <div className="tab-container">
                    {
                        dates.length > 0 && dates[activeTab].data.map(item => <ChannelSceduleItem item={item} key={item.id} setReplaySource={setReplaySource} />)
                    }
                </div>
            </div>
        </div>
    );
});

export default ChannelSchedule;