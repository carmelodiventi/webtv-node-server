import React, { useState } from 'react';
import base64 from 'base-64';
import utf8 from 'utf8';
import { getStartEndTime } from '../../../utils/helpers';

const ChannelSceduleItem = ({ item, setReplaySource }) => {

    const [showDescription, setShowDescription] = useState(false);

    return (
        <div className={`${item.now_playing ? 'epg-data now_playing' : 'epg-data '}`}>
            <div className="epg-content">
                <time>{getStartEndTime(item.start)}</time>
                <time>{getStartEndTime(item.end)}</time>
                <div className="epg-content__item">
                    <h3 className="title" onClick={() => setShowDescription(!showDescription)}>{utf8.decode(base64.decode(item.title))}</h3>
                    <div className={`${showDescription ? 'description showing' : 'description'}`}>
                        {utf8.decode(base64.decode(item.description))}
                    </div>
                </div>
                {item.has_archive === 1 &&
                    <button className="replay" onClick={() => { setReplaySource(item) }}>
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M24 12C24 15.2053 22.7518 18.2188 20.4853 20.4853C18.2188 22.7518 15.2053 24 12 24C8.79474 24 5.78119 22.7518 3.51471 20.4853C1.24823 18.2188 0 15.2053 0 12H1.875C1.875 17.5829 6.41711 22.125 12 22.125C17.5829 22.125 22.125 17.5829 22.125 12C22.125 6.41711 17.5829 1.875 12 1.875C8.34833 1.875 5.0517 3.80365 3.25452 6.89062H6.09375V8.76562H0V2.67188H1.875V5.55432C2.74091 4.1944 3.87543 3.01868 5.22565 2.09363C7.22516 0.723999 9.56763 0 12 0C15.2053 0 18.2188 1.24823 20.4853 3.51471C22.7518 5.78119 24 8.79474 24 12ZM16.9675 12.0266L9 16.6306V7.42236L16.9675 12.0266ZM10.875 13.3817L13.22 12.0266L12.0475 12.7041L10.875 13.3817Z" />
                        </svg>
                    </button>
                }
            </div>
        </div>
    );
};

export default ChannelSceduleItem;