import React, { useState } from 'react';
import base64 from 'base-64';
import utf8 from 'utf8';
import { getTime, getProgress } from '../../utils/helpers';

const Epg = ({ data }) => {

    const { epg_listings } = data;
    const [showingDesc, setShowingDesc] = useState(false);

    return (
        <div className="live__metadata">

            {epg_listings.map(item => {
                return (
                    <div className="live__metadata--item" key={item.id}>
                        <progress value={getProgress(item.start_timestamp, item.stop_timestamp)} max="100" />
                        <div className="live__metadata--item__details">
                            <div>
                                <time>
                                    {getTime(item.start_timestamp)}
                                </time>
                                <time>
                                    {getTime(item.stop_timestamp)}
                                </time>
                                <h3 className="title">
                                    {utf8.decode(base64.decode(item.title))}
                                </h3>
                            </div>
                            {item.description &&
                                <div>
                                    <button className="btn" onClick={ () => setShowingDesc(!showingDesc)}>
                                        info
                                    </button>
                                </div>
                            }
                        </div>
                        <div className={`live__metadata--item__description ${showingDesc ? 'showing' : ''}`}>
                            { utf8.decode(base64.decode(item.description))}
                        </div>
                    </div>
                )
            })}
        </div>
    );
};

export default Epg;