import React, { forwardRef } from 'react';
import Player from '../live/player/Player';
import ChannelSchedule from '../live/ChannelSchedule';

const LivePlayer = forwardRef(({ streamName, streamID, tvArchive, source, epg, channelSchedule, showingSchedule, toggleChannelSchedule, getAllEPGLiveStreams, setReplaySource }, ref) => {

    const { playerRef } = ref;


    return (
        <div className="live--player">
            <div className="live--player__channel">
                <div>
                    <h1 className="live--player__name">{streamName || 'Choose a channel to play...'}</h1>
                </div>
                <div>
                    {source && !showingSchedule &&
                        <button className="btn btn-link" onClick={() => getAllEPGLiveStreams(streamID)}>
                            Schedule
                        </button>
                    }
                    {showingSchedule &&
                        <button className="btn btn-link" onClick={toggleChannelSchedule}>
                            Close
                        </button>
                    }
                </div>
            </div>
            <div className="live--player__container" ref={playerRef}>
                <Player playing={true} source={source} epg={epg} />
                <ChannelSchedule data={channelSchedule} showing={showingSchedule} toggleChannelSchedule={toggleChannelSchedule} tvArchive={tvArchive} setReplaySource={setReplaySource} />
            </div>
        </div>
    );
});

export default LivePlayer;