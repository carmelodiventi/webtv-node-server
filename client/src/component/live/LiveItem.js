import React, { useEffect, useState, useRef } from 'react';
import Hls from 'hls.js';
import { getSourceURL } from '../../utils/helpers';
import ReactPlayer from 'react-player';

const LiveItem = ({ stream_icon, stream_type, stream_id, name }) => {


    const poster = useRef();
    const video = useRef();
    const canvas = useRef();
    const source = getSourceURL(stream_type, stream_id);
    const [playing,setPlaying] = useState(false)
    // const [hls] = useState(new Hls());


    // useEffect(() => {

    //     hls.attachMedia(video.current);

    //     hls.on(Hls.Events.MEDIA_ATTACHED, () => {
    //         hls.loadSource(source);
    //     });

    //     hls.on(Hls.Events.LEVEL_LOADED, () => {
    //         console.log(name)
    //         canvas.current.width = video.current.clientWidth;
    //         canvas.current.height = video.current.clientHeight;
    //         canvas.current.getContext('2d').drawImage(video.current, 0, 0, canvas.current.width, canvas.current.height);
    //     });


    //     return () => {
    //         hls.stopLoad()
    //         hls.detachMedia()
    //         hls.destroy()
    //     }

    // }, [hls, source, name])

    // const onHover = e => {
    //     e.target.play();
    //     e.target.muted = false;
    // }

    // const onLeave = e => {
    //     hls.stopLoad()
    //     e.target.pause();
    //     e.target.muted = true;

    // }

    return (
        <div className="video item">
            <div className="preview">
                <ReactPlayer url={source} playing={playing} onClick={() => setPlaying(!playing)} />
                {/* <video muted autoPlay={false} poster={poster.current} onMouseEnter={onHover} onMouseLeave={onLeave} ref={video} />
                <canvas ref={canvas} /> */}
                <div className="badge"> {stream_type}</div>
            </div>
            <div className="details">
                <h4 className="name"> {name}</h4>
            </div>
        </div>
    );
}



export default LiveItem;
