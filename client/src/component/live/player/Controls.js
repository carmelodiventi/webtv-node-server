import React, { useEffect, useState, useRef, memo } from 'react';
import Epg from '../Epg';

const Controls = memo(({ onFullScreen, fullScreen, epg }) => {

    const videoControls = useRef();
    const timeoutControlBar = useRef();
    const [userIsInActivity, setUserIsInActivity] = useState(false)

    useEffect(() => {
        const videoRef = videoControls.current
        videoRef.addEventListener('mousemove', resetTimer);
        videoRef.addEventListener('touchstart', resetTimer);
        window.addEventListener('orientationchange', goFullScreen)
        startTimer();
        return () => {
            clearTimeout(timeoutControlBar.current);
            videoRef.removeEventListener('mousemove', resetTimer);
            videoRef.removeEventListener('touchstart', resetTimer);
            window.removeEventListener('orientationchange', goFullScreen)
        }
    }, [])

    const startTimer = () => {
        timeoutControlBar.current = setTimeout(goInactive, 5000);
    }

    const resetTimer = e => {
        clearTimeout(timeoutControlBar.current);
        goActive();
    }

    const goInactive = () => {
        setUserIsInActivity(false);
    }

    const goActive = () => {
        setUserIsInActivity(true);
        startTimer();
    }

    const goFullScreen = () => {
        if(window.orientation === -90 && !fullScreen || window.orientation === 90 && !fullScreen) onFullScreen();
    }

    return (
        <div className={`video-controls ${!userIsInActivity ? 'user-in-pause' : ''}`} ref={videoControls}>

            <div className="video-controls-options">
                {fullScreen && <button onClick={onFullScreen} className="video-button video-button-go-back" />}
                {!fullScreen && <button onClick={onFullScreen} className="video-button video-button-fullscreen" />}
            </div>
            <div className="video-controls-elements">
                <div className="video-controls-elements__info">
                    <Epg data={epg} />
                </div>
            </div>
        </div>
    );
});

export default Controls;