import React, { Component } from 'react';
import ReactPlayer from 'react-player'
import Controls from './Controls';


class Player extends Component {

    constructor(props) {
        super(props);
        this.state = {
            url: null,
            pip: false,
            playing: false,
            controls: false,
            light: false,
            volume: 0.8,
            muted: false,
            played: 0,
            loaded: 0,
            duration: 0,
            playbackRate: 1.0,
            loop: false,
            fullScreen: false,
            started: false
        }
    }

    componentDidMount() {
        if (this.props.playing) {
            this.setState({
                playing: this.props.playing
            })
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.playing !== this.props.playing) {
            this.setState({
                playing: this.props.playing
            })
        }
    }

    load = url => {
        this.setState({
            url,
            played: 0,
            loaded: 0,
            pip: false
        })
    }

    playPause = () => {
        this.setState({ playing: !this.state.playing })
    }
    stop = () => {
        this.setState({ url: null, playing: false })
    }
    toggleControls = () => {
        const url = this.state.url
        this.setState({
            controls: !this.state.controls,
            url: null
        }, () => this.load(url))
    }
    toggleLight = () => {
        this.setState({ light: !this.state.light })
    }
    toggleLoop = () => {
        this.setState({ loop: !this.state.loop })
    }
    setVolume = e => {
        this.setState({ volume: parseFloat(e.target.value) })
    }
    toggleMuted = () => {
        this.setState({ muted: !this.state.muted })
    }
    setPlaybackRate = e => {
        this.setState({ playbackRate: parseFloat(e.target.value) })
    }
    togglePIP = () => {
        this.setState({ pip: !this.state.pip })
    }
    onStart = () => {
        console.log('onStart');
        this.setState({
            started: true
        })
    }
    onClose = () => {
        console.log('onClose');
    }
    onPlay = () => {
        console.log('onPlay')
        this.setState({ playing: true })
    }
    onEnablePIP = () => {
        console.log('onEnablePIP')
        this.setState({ pip: true })
    }
    onDisablePIP = () => {
        console.log('onDisablePIP')
        this.setState({ pip: false })
    }
    onPause = () => {
        console.log('onPause')
        this.setState({ playing: false })
    }
    onSeekMouseDown = () => {
        this.setState({ seeking: true })
    }
    onSeekChange = value => {
        this.setState({ played: parseFloat(value) })
    }
    onSeekMouseUp = value => {
        this.setState({ seeking: false })
        this.player.seekTo(value)
    }
    onProgress = state => {
        //console.log('onProgress', state)
        // We only want to update time slider if we are not currently seeking
        if (!this.state.seeking) {
            this.setState(state)
        }
    }

    onClickFullscreen = () => {
        this.setState({
            fullScreen: !this.state.fullScreen
        })
    }


    onEnded = () => {
        this.setState({ playing: this.state.loop })
    }

    onDuration = (duration) => {
        this.setState({ duration })
    }

    ref = player => this.player = player;

    render() {


        const { playing, controls, light, volume, muted, loop, playbackRate, pip, fullScreen, started } = this.state;
        const { source, epg } = this.props;

        return (

            <div className={`video-player ${playing ? 'is-playing' : null} ${fullScreen ? 'is-fullscreen' : null}`}>

                <ReactPlayer
                    ref={this.ref}
                    className='react-player'
                    width='100%'
                    height='100%'
                    url={source}
                    pip={pip}
                    playing={playing}
                    controls={controls}
                    light={light}
                    loop={loop}
                    playbackRate={playbackRate}
                    volume={volume}
                    muted={muted}
                    onReady={() => console.log('onReady')}
                    onStart={this.onStart}
                    onPlay={this.onPlay}
                    onEnablePIP={this.onEnablePIP}
                    onDisablePIP={this.onDisablePIP}
                    onPause={this.onPause}
                    onBuffer={() => console.log('onBuffer')}
                    onSeek={e => console.log('onSeek', e)}
                    onEnded={this.onEnded}
                    onError={e => console.log(`Error: ${e}`)}
                    onProgress={this.onProgress}
                    onDuration={this.onDuration}
                    playsinline
                />

                {started &&
                    <Controls
                        onFullScreen={this.onClickFullscreen}
                        fullScreen={fullScreen}
                        epg={epg}
                    />
                }

                <div className="video-player__mask"></div>

            </div>

        )
    }
}


export default Player;