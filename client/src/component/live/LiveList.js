import React from 'react';
import LiveItem from './LiveItem';


const LiveList = ({ items }) => {

    return (
        <div className="section-side">
            <ul>
                {items.map(item => <LiveItem data={item} key={item.stream_id} />)}
            </ul>
        </div>
    );

}

export default LiveList;