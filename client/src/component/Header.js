import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { user } from '../utils/auth';
import { logout } from '../actions/index';
import Nav from './Nav';
import Logo from './Logo';
import User from './User';


const Header = ({ logout, auth }) => {

  let { username } = user();

  if (auth.isAuthenticated) {
    return (
      <div className="page-header">
        <header>
          <Logo />
          <Nav />
          <User username={username} logout={logout} />
        </header>
      </div>
    );
  }

  return null;

}


function mapStateToProps(state) {
  return {
    auth: state.authentication
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ logout }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);