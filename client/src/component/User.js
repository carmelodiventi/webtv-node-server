import React from 'react';
import { Link } from 'react-router-dom';
import { IoIosLogOut } from 'react-icons/io'
const User = ({ username, logout }) => {

    const getInitial = (username) => {

        const initial = username.split(" ").reduce((prev, curr) => {
            return prev + curr.charAt(0)
        }, '')

        return initial;
    }

    return (
        <div>

            <Link to="/account" className="user-info">
                {getInitial(username)}
            </Link>

            <button className="user-info" alt="logout" onClick={logout}>
                <IoIosLogOut />
            </button>

        </div>
    );
};

export default User;