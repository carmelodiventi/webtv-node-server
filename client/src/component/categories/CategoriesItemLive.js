import React from 'react';

const CategoriesItemLive = ({ type, id, name, icon, tvArchive, nowPlaying, onClick }) => {

    return (
        <li className={`${nowPlaying === id ? 'playing' : ''}`}>
            <span onClick={() => onClick(name, type, id, tvArchive)}>
                <div style={{ backgroundImage: `url(${icon})` }} className="icon" />
                {name.toLowerCase()}
            </span>
        </li>
    );
};

export default CategoriesItemLive;