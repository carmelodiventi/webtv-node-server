import React from 'react';
import { Link, withRouter } from 'react-router-dom';

const CategoriesItem = ({ name, id, match: { path } }) => {

    return (
        <li>
            <Link to={`${path}/category/${id}`}>
                {name.toLowerCase()}
            </Link>
        </li>
    );
};

export default withRouter(CategoriesItem);