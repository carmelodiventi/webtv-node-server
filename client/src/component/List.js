import React from 'react';

class List extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            items: 20,
            loading: false
        };
    }

    componentDidMount() {
        // Detect when scrolled to bottom.
        this.refs.myscroll.addEventListener("scroll", () => {
            if (
                this.refs.myscroll.scrollTop + this.refs.myscroll.clientHeight >=
                this.refs.myscroll.scrollHeight
            ) {
                this.loadMore();
            }
        });
    }

    showItems() {
        var items = [];
        for (var i = 0; i < this.state.items; i++) {
            items.push(<this.props.item/>);
        }
        return items;
    }

    loadMore() {
        this.setState({ loading: true });
        setTimeout(() => {
            this.setState({ items: this.state.items + 20, loading: false });
        }, 2000);
    }

    render() {
        return (
            <div
                class={this.props.className}
                style={{ height: "520px", overflow: "auto" }}
                ref="myscroll">
                {this.showItems()}
                {this.state.loading
                    ? <p className="App-intro">
                        loading ...
                        </p>
                    : ""}
            </div>
        );
    }
};

export default List;