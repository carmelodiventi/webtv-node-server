import React, { forwardRef } from 'react';

const SectionSide = forwardRef(({ children }, ref) => {
    return (
        <div className="section-side" ref={ref}>
            {children}
        </div>
    );
});

export default SectionSide;