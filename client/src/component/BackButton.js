import React, { forwardRef } from 'react';

const BackButton = forwardRef(({ goBack, text }, backButton) => {

    return (
        <div className="back-button" ref={backButton}>
            <button onClick={goBack}>
                <svg width="12" height="14" viewBox="0 0 12 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M9.5399e-08 7L12 0.0717966L12 13.9282L9.5399e-08 7Z"/>
                </svg>
                {text}
            </button>
        </div>
    );
});

BackButton.defaultProps = {
    text: "Go back..."
}

export default BackButton;