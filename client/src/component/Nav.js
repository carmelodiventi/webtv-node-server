import React, { useState, useEffect, useRef } from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { gsap } from 'gsap';
import { IoIosMenu } from 'react-icons/io';


const Nav = () => {

  const [navOpen, setNavOpen] = useState(false);
  const [animation] = useState(gsap.timeline({ paused: true }));
  const menu = useRef();
  const history = useHistory();

  useEffect(() => {

    history.listen(() => {
      onRouteChange()
    });

    animation
    .to(menu.current, {
      x: 0,
      opacity: 1,
      duration: 1
    })
    .to(menu.current.firstChild.children,{
      y: 0,
      opacity: 1,
      stagger: .1
    })
    .reverse()

  }, [animation, history])

  useEffect( () => {
    animation.reversed(!navOpen)
  },[navOpen, animation])

  const onRouteChange = () => {
    if(window.innerWidth <= 769){
      setNavOpen(false);
    }
  }

  const onToggle = () => {
    setNavOpen(!navOpen);
  }

  return (
    <>
      <nav className={`${navOpen ? 'main-nav showing' : 'main-nav'}`} ref={menu}>

        <ul className="menu">
          <li>
            <NavLink to="/" exact className="category" activeClassName="active">
              <span>Featured</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/live" exact className="category" activeClassName="active">
              <span>Live</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/movies" exact className="category" activeClassName="active">
              <span>Vod</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/series" exact className="category" activeClassName="active">
              <span>Series</span>
            </NavLink>
          </li>
        </ul>

      </nav>

      <button className="menu-icon" onClick={onToggle}>
        <IoIosMenu/>
      </button>
    </>
  );
};

export default Nav;