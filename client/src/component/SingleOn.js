import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import { withToastManager } from 'react-toast-notifications';
import { login } from '../actions/index';
import Loader from './Loader';

class SingleOn extends Component {

  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: ''
    }

    this.login = this.login.bind(this);
  }

  componentDidMount() {
    const { match: { params } } = this.props;
    this.login(params.username, params.password);
  }

  login = async (username, password) => {

    const { toastManager } = this.props;

    if (username && password) {

      const credentials = {
        username,
        password
      }

      this.props.login(credentials).then(res => {

        if (res.error) {

          this.setState({
            error: true,
            error_message: `Login failed ${res.error.message}`,
            redirectToReferrer: false,
          })

          toastManager.add(res.error.message, {
            appearance: 'error'
          })

        }
        else {
          this.props.history.push('/');
        }
      });


    }

  };

  render() {

    return (
      <div className="login">

        <Link to='/' className="category logo-login"></Link>

        {this.state.error &&
          <div className="error_message">

            <p className="alert alert-error">
              {this.state.error_message}
              <br />
            </p>

            <Link to='/login' className="btn btn-primary">Go to Login Page</Link>
          </div>
        }

        <Loader animating={this.props.auth.loading} />
      </div>
    )
  }

}

function mapStateToProps(state) {
  return {
    auth: state.authentication
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ login }, dispatch);
}


export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withToastManager
)(SingleOn);