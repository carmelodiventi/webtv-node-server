import React, { useState, useEffect, useRef } from 'react';

const MovieImage = ({ stream_icon }) => {

    const [loading, setLoading] = useState(true);
    const src = stream_icon || require('../../img/image_not_found.jpg');
    const image = useRef(new Image());

    useEffect(() => {
        image.current.src = src;
        image.current.onload = () => {
            setLoading(false);
        };
        image.current.onerror = () => {
            setLoading(false);
        };
    }, [image, src])


    return (
        <div className="featured-image" style={{ backgroundImage: `url(${image.current.src})` }}>
            {loading &&
                <div className="loading-image">
                    <div className="spin"></div>
                </div>
            }
        </div>
    );
};

export default MovieImage;