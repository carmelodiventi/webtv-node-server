import React from 'react';
import { withRouter } from 'react-router-dom';
import MovieImage from './MovieImage';

const MovieItem = ({ data: { stream_icon, stream_id, name }, history }) => {

    const getMovieInfo = () => {
        history.push(`/movies/${stream_id}`)
    }

    return (
        <div className="item" onClick={getMovieInfo}>
            <MovieImage stream_icon={stream_icon}/>
            <div className="details">
                <h4>{name}</h4>
            </div>
        </div>
    );
}



export default withRouter(MovieItem);
