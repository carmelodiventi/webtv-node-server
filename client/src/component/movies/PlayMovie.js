import React, { useState } from 'react';
import PlayButton from '../vod/player/PlayButton';

const PlayMovie = ({ playMovie }) => {

    const [isHover, setIsHover] = useState(false);

    return (
        <div>
            <button className="btn btn-primary" onClick={playMovie} onMouseOver={() => setIsHover(true)} onMouseOut={() => setIsHover(false)}>
                <PlayButton hover={isHover} /> Play Movie
            </button>
        </div>
    );
};

export default PlayMovie;