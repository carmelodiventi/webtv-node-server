import React from 'react';

const InfoItem = ({label, text}) => {
    return (
        <li>
            <label className="evidenced">{label}</label>
            <span>{text}</span>
        </li>
    );
}

export default InfoItem;