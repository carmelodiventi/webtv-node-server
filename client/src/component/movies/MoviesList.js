import React, { useState, useEffect } from 'react';
import MovieItem from './MovieItem';
import { FixedSizeList as List } from "react-window";
import AutoSizer from "react-virtualized-auto-sizer";
import { chunk } from '../../utils/helpers';


const MoviesList = ({ data }) => {

    const [items, setItems] = useState([]);

    useEffect(() => {

        window.addEventListener('resize', chunkItems);

        return () => {
            window.removeEventListener('resize', chunkItems);
        }

    }, [])

    useEffect(() => {
        chunkItems()
    }, [data])

    const chunkItems = () => {
        let itemCount = 0;
        if (window.innerWidth < 768) {
            itemCount = 2;
        }
        else if (window.innerWidth > 768 && window.innerWidth < 1024) {
            itemCount = 4;
        }
        else if (window.innerWidth > 1024) {
            itemCount = 6;
        }

        setItems(chunk(data, itemCount));
    }



    return (
        <div className="items">
            <AutoSizer>
                {({ height, width }) => (
                    <List
                        className="items-list"
                        height={height}
                        itemCount={items.length}
                        itemSize={height / 2}
                        width={width}>
                        {({ index, style }) => {

                            return (
                                <div className="items-list-row" style={style}>
                                    {items[index].map(item => <MovieItem data={item} key={item.stream_id} />)}
                                </div>
                            )

                        }}
                    </List>
                )}
            </AutoSizer>
        </div>
    );

}

export default MoviesList;