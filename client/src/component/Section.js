import React, { forwardRef } from 'react';

const Section = forwardRef(({ text, children }, ref) => {
    return (
        <div className="section-main" ref={ref}>
            {text &&
                <h1 className="section-main__title">
                    {text}
                </h1>
            }
            {children}
        </div>
    );
});

export default Section;