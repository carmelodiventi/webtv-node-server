import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import screenfull from 'screenfull';
import ReactPlayer from 'react-player'
import Controls from './Controls';

class Player extends Component {

    constructor(props) {
        super(props);
        this.state = {
            url: null,
            pip: false,
            playing: false,
            controls: false,
            light: false,
            volume: 0.8,
            muted: false,
            played: 0,
            loaded: 0,
            duration: 0,
            playbackRate: 1.0,
            loop: false,
            showing: false,
        }
    }

    componentDidMount() {
        if (this.props.showing) {
            this.setState({
                showing: this.props.showing
            })
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.showing !== this.props.showing) {
            this.setState({
                showing: this.props.showing,
                playing: this.props.showing
            })
        }
    }

    load = url => {
        this.setState({
            url,
            played: 0,
            loaded: 0,
            pip: false
        })
    }

    playPause = () => {
        this.setState({ playing: !this.state.playing })
    }
    stop = () => {
        this.setState({ url: null, playing: false })
    }
    toggleControls = () => {
        const url = this.state.url
        this.setState({
            controls: !this.state.controls,
            url: null
        }, () => this.load(url))
    }
    toggleLight = () => {
        this.setState({ light: !this.state.light })
    }
    toggleLoop = () => {
        this.setState({ loop: !this.state.loop })
    }
    setVolume = e => {
        this.setState({ volume: parseFloat(e.target.value) })
    }
    toggleMuted = () => {
        this.setState({ muted: !this.state.muted })
    }
    setPlaybackRate = e => {
        this.setState({ playbackRate: parseFloat(e.target.value) })
    }
    togglePIP = () => {
        this.setState({ pip: !this.state.pip })
    }
    onStart = () => {
       console.log('onStarted')
    }
    onClose = () => {
        this.setState({
            showing: false,
            playing: false 
        }, () => {
            this.props.togglePlayer()
        })
        console.log('onClose');
    }
    onPlay = () => {
        console.log('onPlay')
        this.setState({ playing: true })
    }
    onEnablePIP = () => {
        console.log('onEnablePIP')
        this.setState({ pip: true })
    }
    onDisablePIP = () => {
        console.log('onDisablePIP')
        this.setState({ pip: false })
    }
    onPause = () => {
        console.log('onPause')
        this.setState({ playing: false })
    }
    onSeekChange = value => {
        this.setState({ played: parseFloat(value) })
        this.player.seekTo(value)
    }
    onProgress = state => {
        //console.log('onProgress', state)
        // We only want to update time slider if we are not currently seeking
        if (!this.state.seeking) {
            this.setState(state)
        }
    }
    onClickFullscreen = () => {
        screenfull.request(findDOMNode(this.player))
    }

    onEnded = () => {
        this.setState({ playing: this.state.loop })
    }

    onDuration = (duration) => {
        this.setState({ duration })
    }

    ref = player => {
        this.player = player;
    }

    render() {

        const { playing, controls, light, volume, muted, loop, played, loaded, showing, duration, playbackRate, pip, seeking } = this.state;
        const { source } = this.props;

        return (

            <div className={` video-player ${playing ? 'is-playing' : ''} ${showing ? 'showing' : 'hidding'}`} ref={this.videoPlayer}>

                <ReactPlayer
                    ref={this.ref}
                    className='react-player'
                    width='100%'
                    height='100%'
                    url={source}
                    pip={pip}
                    playing={playing}
                    controls={controls}
                    light={light}
                    loop={loop}
                    playbackRate={playbackRate}
                    volume={volume}
                    muted={muted}
                    onReady={() => console.log('onReady')}
                    onStart={this.onStart}
                    onPlay={this.onPlay}
                    onEnablePIP={this.onEnablePIP}
                    onDisablePIP={this.onDisablePIP}
                    onPause={this.onPause}
                    onBuffer={() => console.log('onBuffer')}
                    onSeek={e => console.log('onSeek', e)}
                    onEnded={this.onEnded}
                    onError={e => console.log(`Error: ${e}`)}
                    onProgress={this.onProgress}
                    onDuration={this.onDuration}
                    playsinline
                />

                
                <Controls
                    goBack={this.onClose}
                    playPause={this.playPause}
                    seeking={seeking}
                    loaded={loaded}
                    played={played}
                    playing={playing}
                    duration={duration}
                    onSeekChange={this.onSeekChange}
                />

            </div>

        )
    }
}


export default Player;