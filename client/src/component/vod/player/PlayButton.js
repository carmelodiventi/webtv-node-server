import React, { useEffect } from 'react';
import { Motion, spring } from "react-motion";

const PlayButton = props => {

    const { hover, ...others } = props;
    
    useEffect( () => {
    
    },[hover])

    const animate = (val) => spring((val),{
        stiffness:120,
        damping: 35
    })
    
    return (
        <Motion style={{ offset: hover ? animate(0) : animate(271) }}>
            {({ offset }) =>
                <svg 
                {...others}
                width="25" height="25" viewBox="-4 -10 57 106" fill="none">
                    <polygon strokeDasharray={271} strokeDashoffset={offset} stroke="white" strokeWidth="8" points="0 42.9371985 0 -0.125603023 46.97776016 42.9371985 0 86" />
                    <polygon stroke="white" strokeWidth="3" points="0 42.9371985 0 -0.125603023 46.97776016 42.9371985 0 86" />
                </svg>
            }
        </Motion>
    )

}

export default PlayButton;