import React, { useEffect, useRef, useState } from 'react';

const ProgressBar = ({ seeking, played, onSeekChange }) => {

    const progressBar = useRef();
    const [progress, setProgress] = useState(0);

    useEffect(() => {
        const progressRef = progressBar.current;
        progressRef.addEventListener('click', _onSeekChange);
        progressRef.addEventListener('mousemove', _onMouseMove);
        progressRef.addEventListener('touchstart', _onSeekChange);

        return () => {
            progressRef.removeEventListener('click', _onSeekChange);
            progressRef.addEventListener('mousemove', _onMouseMove);
            progressRef.removeEventListener('touchstart', _onSeekChange);
        }

    }, [])


    const _onSeekChange = e => {
        if (seeking) return;
        let posX = e.pageX - e.target.offsetLeft;
        let seek = parseFloat(posX / e.target.offsetWidth);
        onSeekChange(seek)
    }

    const _onMouseMove = e => {
        let posX = e.pageX - e.target.offsetLeft;
        if(posX < 0){
            posX = 0;
        }
        else if(posX >= e.target.offsetWidth){
            posX = e.target.offsetWidth;
        }
        let seek = parseFloat(posX / e.target.offsetWidth);
        setProgress(posX);
       
    }

    return (
        <div>
            <progress value={played} max="1" ref={progressBar} style={ {'--cursorX' : `${progress}px`} } />
        </div>
    );
};

export default ProgressBar;