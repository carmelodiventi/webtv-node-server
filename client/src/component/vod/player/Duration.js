import React from 'react';
import { getDuration } from '../../../utils/helpers';

const Duration = ({seconds}) => {
    return (
        <div>
            {getDuration(seconds)}
        </div>
    );
};

export default Duration;