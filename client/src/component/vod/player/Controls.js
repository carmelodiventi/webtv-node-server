import React, { useEffect, useState, useRef } from 'react';
import Duration from './Duration';
import ProgressBar from './ProgressBar';

const Controls = ({ goBack, playPause, playing, duration, played, seeking, loaded, onSeekChange, onSeekMouseDown, onSeekMouseUp }) => {

    const timeoutControlBar = useRef();
    const videoControls = useRef();
    const [userIsInActivity, setUserIsInActivity] = useState(false)

    useEffect(() => {
        const videoControlsRef = videoControls.current;
        videoControlsRef.addEventListener('mousemove', resetTimer);
        videoControlsRef.addEventListener('touchstart', resetTimer);
        startTimer();
        return () => {
            clearTimeout(timeoutControlBar.current);
            videoControlsRef.removeEventListener('mousemove', resetTimer);
            videoControlsRef.removeEventListener('touchstart', resetTimer);
        }
    },[])

    const startTimer = () => {
        timeoutControlBar.current = setTimeout(goInactive, 3000);
    }

    const resetTimer = e => {
        clearTimeout(timeoutControlBar.current);
        goActive();
    }

    const goInactive = () => {
        setUserIsInActivity(false);
    }

    const goActive = () => {
        setUserIsInActivity(true);
        startTimer();
    }

    return (
        <div className={`video-controls ${!userIsInActivity ? 'user-in-pause' : ''}`} ref={videoControls}>
            <div className="video-controls-options">
                <button onClick={goBack} className="video-button video-button-go-back" />
            </div>
            <div className="video-controls-elements">
                <div className="video-controls-elements__buttons">
                    <button onClick={playPause} className={`video-button ${playing ? 'video-button-pause' : 'video-button-play'}`} />
                </div>
                <div className="video-controls-elements__info">
                    <Duration seconds={duration * played} />
                    <ProgressBar className='status-bar' seconds={duration * played} seeking={seeking} played={played} duration={duration} loaded={loaded} onSeekChange={onSeekChange} onSeekMouseDown={onSeekMouseDown} onSeekMouseUp={onSeekMouseUp} />
                    <Duration seconds={duration} />
                </div>
            </div>
        </div>
    );
};

export default Controls;