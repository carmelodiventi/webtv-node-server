import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import { withToastManager } from 'react-toast-notifications';
import { Redirect } from "react-router-dom";
import { login } from '../actions/index';
import Loader from './Loader';

class Login extends Component {

  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: ''
    }
  }


  handleInputChange = (event) => {
    const name = event.target.name;
    this.setState({
      [name]: event.target.value
    })
  }


  login = (event) => {

    const { toastManager } = this.props;
    
    if (event.target.username.value && event.target.password.value) {
      event.preventDefault();

      const credentials = {
        username: event.target.username.value,
        password: event.target.password.value
      }

      this.props.login(credentials).then(res => {
        if (res.error) {
          toastManager.add(res.error.message, {
            appearance: 'error'
          })
        }
      });


    }

  };

  render() {

    const { from } = this.props.location.state || { from: { pathname: "/" } };

    if (this.props.auth.isAuthenticated) {
      return <Redirect to={from} />;
    }

    return (
      <div className="login">

        <form onSubmit={this.login}>
          <div className="form-group">
            <input type="text"
              onChange={this.handleInputChange}
              value={this.state.username}
              name="username"
              className="form-control"
              placeholder="username" required />
          </div>
          <div className="form-group">
            <input
              onChange={this.handleInputChange}
              value={this.state.password}
              type="password"
              name="password"
              className="form-control"
              placeholder="password" required />
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-primary"><span>Log in</span></button>
          </div>
        </form>

        <Loader animating={this.props.auth.loading} />

      </div>
    )
  }

}

function mapStateToProps(state) {
  return {
    auth: state.authentication
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ login }, dispatch);
}


export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withToastManager
)(Login);