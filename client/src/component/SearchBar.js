import React, { Component } from 'react';
import { Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {user} from '../utils/auth';
import {getExpDate} from '../utils/helpers';

class SearchBar extends Component {

constructor(props){
	super(props);
    this.state = {
      loading: false,
      showing : false,
      items : [],
      initialItems : []
    }
    //this.filterList = this.filterList.bind(this);
    this.toggleList = this.toggleList.bind(this);

} 

toggleList(event){
	this.setState({
	  showing : !this.state.showing
	});
}

/*
filterList(event){
    var updatedList = this.state.initialItems;
    updatedList = updatedList.filter((item) => {
      var channel = item.title;
      return channel.toLowerCase().search(event.target.value.toLowerCase()) !== -1;
    });
    this.setState({
    	items: updatedList,
    	showing : true
    });
}
*/

setSource(source){
	this.props.source(source);
	this.setState({
	  showing : false
	});
}

onSetResult = (result, key) => {
    	sessionStorage.setItem(key, JSON.stringify(result));
}

/*
componentDidMount() {
    const cachedPlayList = sessionStorage.getItem('playlist');
	if (cachedPlayList) {
		this.setState({
			items : JSON.parse(cachedPlayList),
			initialItems : JSON.parse(cachedPlayList),
			loading : false
		});
	}
	else{
		const url = config.PROXY_URL + config.PLAYLIST_URL;
		getPlaylist(url).then((data)=>{
    	  const playlist = data.split(/\r?\n/).join('\n');
	      const parsed = m3uParser.parse(data);
	      if(typeof parsed !== null && typeof parsed === 'object'){
	      		this.setState({
	              loading: false,
	              items: parsed.tracks,
	              initialItems: parsed.tracks
	        	});
	        	this.onSetResult(parsed.tracks, 'playlist');
	      }
    });
	}
}
*/

 render(){

 		SearchBar.propTypes = {
	      onCloseRequest: PropTypes.func,
	      children: PropTypes.oneOfType([
	        PropTypes.arrayOf(PropTypes.node),
	        PropTypes.node,
	      ])
        };

	 	let listItems;
	 	if(this.state.items !== undefined){
	 		listItems = [];
	 		this.state.items.map( (item, index) => {
	 			listItems.push(<li key={index} onClick={() => this.setSource(item.file)}>{item.title}</li>);
	 		});
	 	}

		return(

			 <div className="top-bar">
			 	<div className="top-menu">
			 	 	<span>Welcome {user().username}</span>
			 	 	<span data-title="expired date">{getExpDate(user().exp_date)}</span>
			 	 	<Link to='/account'>Account</Link>
			 	</div>
			 	<div className="top-search">
	                <input type="text" placeholder="Search for a channel, TV Show or movie that you are looking for" onChange={this.props.filterList}/>
	                { this.state.showing &&
		                <div className="search-content">
		                	<ul>
		                		{listItems}
		                	</ul>
		                </div>
	                }
                </div>
                { this.state.loading && <div className="loader"><div className="icon"></div></div> }
              </div>
         )
 }

}

export default SearchBar;