import React from 'react';

const Loader = ({ animating }) => {

    if (animating) {
        return (
            <div className="loader"><div className="icon"></div></div>
        );
    }

    return null;
};

export default Loader;