import axios from 'axios';

export const checkAuth = () => next => action => {
 

  axios.interceptors.response.use((response) => {
    // do something with the response data
    console.log(response);
    return response;
  
  }, error => {
  
    let res = error.response;
    if (res.status === 404 && res.config && !res.config.__isRetryRequest) {
      next(action);
      localStorage.removeItem('server');
      localStorage.removeItem('user');
      window.history.go('/login');
    }
    // handle the response error
    
    return Promise.reject(error);
  });

  next(action);



};
