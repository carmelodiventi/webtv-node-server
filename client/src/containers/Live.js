import React, { Component, createRef } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import { withToastManager } from 'react-toast-notifications';
import { gsap } from 'gsap';
import Layout from '../component/Layout';
import { getLiveStreamCategories } from '../actions'
import Section from '../component/Section';
import SectionSide from '../component/SectionSide';
import Loader from '../component/Loader';
import Search from '../component/search/Search';
import CategoriesItem from '../component/categories/CategoriesItem';


class Live extends Component {

	constructor(props) {
		super(props);
		this.state = {
			items: [],
			initialItems: []
		}
		this.animation = gsap.timeline({ paused: true });
		this.section = createRef();
		this.side = createRef();
	}

	componentDidMount() {
		const { toastManager } = this.props;
		this.props.getLiveStreamCategories().then(res => {
			if (res.error) {
				toastManager.add(res.error.message, {
					appearance: 'error'
				})
			}
		});

		this.animation
			.to(this.section.current,
				{
					duration: 1,
					y: 0,
					opacity: 1
				})
			.to(this.side.current,
				{
					duration: 1,
					x: 0,
					opacity: 1
				})
			.play()

	}

	componentDidUpdate(prevProps) {
		if (this.props.live.categories !== prevProps.live.categories) {
			this.setState({
				items: this.props.live.categories,
				initialItems: this.props.live.categories
			})
		}
	}

	mapItems = items => {
		return items.map(item => ({ ...item, id: item.category_id, name: item.category_name }))
	}

	filterBy = e => {

		let currentItems = this.state.items;
		let filteredItems = [];

		if (e.target.value !== '') {

			currentItems = this.state.initialItems;

			filteredItems = currentItems.filter(item => {
				let name = item.category_name.toLowerCase();
				let filter = e.target.value.toLowerCase();
				return name.includes(filter);
			})

			this.setState({
				items: filteredItems
			})

		}

		else {

			this.setState({
				items: this.state.initialItems
			})

		}


	}

	render() {

		return (
			<Layout>
				<div className="categories">
					<div className="section">
						<Section text="Choose a Category" ref={this.section} />
						<SectionSide ref={this.side}>
							<Search filterBy={this.filterBy} />
							<ul className="categories">
								{this.mapItems(this.state.items).map(({ id, name }) => <CategoriesItem name={name} id={id} key={id} />)}
							</ul>
						</SectionSide>
					</div>
				</div>
				<Loader animating={this.props.live.loading} />
			</Layout>
		);
	}
}


function mapStateToProps(state) {
	return {
		live: state.live
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({ getLiveStreamCategories }, dispatch);
}

export default compose(
	connect(mapStateToProps, mapDispatchToProps),
	withToastManager
)(Live);