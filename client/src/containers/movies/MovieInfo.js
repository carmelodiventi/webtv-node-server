import React, { Component, createRef } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { gsap } from 'gsap';
import { getFullYear, getDuration, getSourceURL } from '../../utils/helpers';
import { getVODInfo, closeVODInfo } from '../../actions/index';
import { withRouter } from 'react-router';
import InfoItem from '../../component/movies/InfoItem';
import Player from '../../component/vod/player/Player';
import Loader from '../../component/Loader';
import PlayMovie from '../../component/movies/PlayMovie';
import BackButton from '../../component/BackButton';
import Layout from '../../component/Layout';


class MovieInfo extends Component {

    constructor(props) {
        super(props);
        this.animation = gsap.timeline({ paused: true })
        this.playerAnimation = gsap.timeline({ paused: true })
        this.player = createRef();
        this.title = createRef();
        this.info = createRef();
        this.plot = createRef();
        this.cover = createRef();
        this.backButton = createRef();
        this.state = {
            showPlayer: false
        }
    }

    componentDidMount() {

        const { stream_id } = this.props.match.params;
        this.props.getVODInfo(stream_id);

        this.animation
            .to(this.title,
                {
                    duration: .5,
                    opacity: .2
                }
            )
            .to([this.info, this.plot, this.cover, this.backButton],
                {
                    duration: 1,
                    y: 0,
                    opacity: 1,
                    stagger: .2
                })
            .play()

    }

    componentWillUnmount() {
        this.props.closeVODInfo();
    }

    togglePlayer = () => {
        this.setState({
            showPlayer: !this.state.showPlayer
        })
    }

    goBack = () => {
        this.animation.reverse().then(() => {
            this.props.history.goBack()
        });
    }

    render() {

        const { info, movie_data } = this.props.movies.vod_info;
        const { showPlayer } = this.state;
        let source = null;

        if (movie_data.length !== 0) {
            const { stream_id, container_extension } = movie_data;
            source = getSourceURL("movie", stream_id, container_extension);
        }

        const youtube_trailer = info.youtube_trailer ? `https://youtu.be/${info.youtube_trailer}` : null;

        return (
            <Layout>
                <div className="vod-info">

                    <div className="section-title" ref={el => this.title = el}>
                        <span className="title">{movie_data.name}</span>
                    </div>

                    <div className="description">
                        <div className="more-info">
                            <div className="info" ref={el => this.info = el}>
                            <ul>

                                {info.genre &&
                                    <InfoItem label="Genre" text={info.genre} />
                                }

                                {info.cast &&
                                    <InfoItem label="Cast" text={info.cast} />
                                }

                                {info.country &&
                                    <InfoItem label="Country" text={info.country} />
                                }

                                {info.director &&
                                    <InfoItem label="Director" text={info.director} />
                                }

                                {info.duration_secs &&
                                    <InfoItem label="Duration" text={getDuration(info.duration_secs)} />
                                }

                                {info.releasedate &&
                                    <InfoItem label="Anno" text={getFullYear(info.releasedate)} />
                                }

                                {info.rating &&
                                    <InfoItem label="Rating" text={info.rating} />
                                }
                            </ul>
                        </div>
                            <div className="plot" ref={el => this.plot = el}>

                            {movie_data && movie_data.name ? <span className="title">{movie_data.name}</span> : <span className="title"> title not found </span>}

                            {info.plot &&
                                <p className="plot-text">{info.plot}</p>
                            }

                            <div className="btn-group">

                                <PlayMovie playMovie={this.togglePlayer} />

                                {youtube_trailer &&
                                    <div>
                                        <a href={youtube_trailer} className="btn btn-warning btn-outline" target="_new">Watch Trailer</a>
                                    </div>
                                }
                            </div>

                        </div>
                        </div>
                        <div className="cover" ref={el => this.cover = el}>
                            {info.movie_image &&
                                <img src={info.movie_image} alt="" />
                            }
                        </div>
                    
                    </div>
                   
                    <Player source={source} showing={showPlayer} togglePlayer={this.togglePlayer} />
                    <Loader animating={this.props.movies.loading} />
                    <BackButton goBack={this.goBack} ref={el => this.backButton = el} />

                </div>
            </Layout>
        );

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getVODInfo, closeVODInfo }, dispatch);
}

function mapStateToProps(state) {
    return {
        movies: state.movies
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MovieInfo));
