import React, { Component, createRef } from 'react';
import { withRouter } from 'react-router';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { gsap } from 'gsap';
import { bindActionCreators } from 'redux';
import { getVODStreams } from '../../actions'
import Layout from '../../component/Layout';
import Loader from '../../component/Loader';
import MoviesList from '../../component/movies/MoviesList';
import Search from '../../component/search/Search';
import BackButton from '../../component/BackButton';

class MoviesCategory extends Component {

	constructor(props) {
		super(props);
		this.state = {
			items: [],
			initialItems: []
		}
		this.animation = gsap.timeline({ paused: true });
		this.backButton = createRef();
	}


	componentDidMount() {
		const { category_id } = this.props.match.params;
		this.props.getVODStreams(category_id);

		this.animation
		.to([this.backButton],
			{
				duration: 1,
				y: 0,
				opacity: 1,
				stagger: .2
			})
		.play()

	}

	componentDidUpdate(prevProps) {
		if (this.props.movies.items !== prevProps.movies.items) {
			this.setState({
				items: this.props.movies.items,
				initialItems: this.props.movies.items
			})
		}
	}

	filterBy = e => {

		let currentItems = this.state.items;
		let filteredItems = [];

		if (e.target.value !== '') {

			currentItems = this.state.initialItems;

			filteredItems = currentItems.filter(item => {
				let name = item.name.toLowerCase();
				let filter = e.target.value.toLowerCase();
				return name.includes(filter);
			})

			this.setState({
				items: filteredItems
			})

		}

		else {

			this.setState({
				items: this.state.initialItems
			})

		}

	}

	goBack = () => {
        this.animation.reverse().then(() => {
            this.props.history.goBack()
        });
    }

	render() {

		return (
			<Layout>
				<Search filterBy={this.filterBy} />
				<MoviesList data={this.state.items} />
				<BackButton text={"back to categories"} goBack={this.goBack} ref={ el => this.backButton = el }/>
				<Loader animating={this.props.movies.loading} />
			</Layout>
		);
	}
}

function mapStateToProps(state) {
	return {
		movies: state.movies
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({ getVODStreams }, dispatch);
}

export default compose(
	withRouter,
	connect(mapStateToProps, mapDispatchToProps)
)(MoviesCategory);