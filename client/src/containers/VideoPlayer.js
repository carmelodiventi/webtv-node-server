import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Player,ControlBar,BigPlayButton } from 'video-react';
import HLSSource from '../component/HLSSource';
import ClosedCaptionButton from './videoplayer/ClosedCaptionButton';
import VideoDetails from './videoplayer/VideoDetails';
import VodDetails from './videoplayer/VodDetails';
import Schedule from './Schedule';
import SourceBar from './SourceBar'; 

class VideoPlayer extends Component{
  constructor(props){
      super(props);

      this.state = {
        videoInFullScreenMode:false,
        source_info:false,
        getInfo:false,
        getSchedule:false,
        showDescription:false,
        player:{
            isFullscreen:false,
            hasStarted:false,
            paused:true,
        }
      }
    
      this.getInfo = this.getInfo.bind(this);
      this.toggleFullscreen = this.toggleFullscreen.bind(this);
      this.play = this.play.bind(this);
      this.pause = this.pause.bind(this);
     
  } 

  componentDidMount() {
    // subscribe state change
    this.refs.player.subscribeToStateChange(this.handleStateChange.bind(this));
  }
   
  componentDidUpdate(prevProps) {
    if (prevProps.videoResource.source && this.props.videoResource.source !== prevProps.videoResource.source) {
       this.refs.player.load();
       this.refs.player.subscribeToStateChange(this.handleStateChange.bind(this));
       this.setState({
          getSchedule:false,
          getInfo:false,
          player:{
            isFullscreen:false,
            hasStarted:false,
            paused:true,
        }
       })
    }
  }


  handleStateChange(state, prevState) {
    // copy player state to this component's state
    this.setState({
      player: state
    });
  }

  play() {
    this.refs.player.play();
  }

  toggleFullscreen() {
    this.refs.player.toggleFullscreen();
  }

  pause() {
    this.refs.player.pause();
  }

  load() {
    this.refs.player.load();
  }

  changeCurrentTime(seconds) {
    return () => {
      const { player } = this.refs.player.getState();
      const currentTime = player.currentTime;
      this.refs.player.seek(currentTime + seconds);
    };
  }

  seek(seconds) {
    return () => {
      this.refs.player.seek(seconds);
    };
  }

  changePlaybackRateRate(steps) {
    return () => {
      const { player } = this.refs.player.getState();
      const playbackRate = player.playbackRate;
      this.refs.player.playbackRate = playbackRate + steps;
    };
  }

  changeVolume(steps) {
    return () => {
      const { player } = this.refs.player.getState();
      const volume = player.volume;
      this.refs.player.volume = volume + steps;
    };
  }

  setMuted(muted) {
    return () => {
      this.refs.player.muted = muted;
    };
  }

  getInfo(){
    this.setState({
      getInfo: !this.state.getInfo
    })
  }

  render(){

    const type = this.props.videoResource.type;
    const source = this.props.videoResource.source;
    const autoPlay = this.props.videoResource.type === "live" || this.props.videoResource.type === 'series' ? true :false;
    const isFullscreen = !this.state.player.isFullscreen ? 'fullscreen' : '';

    if(!this.state.player.hasStarted){
      return false;
    }

    return(
        <div className={`fullscreen`}>
          <Player ref="player" fluid={false} autoPlay={autoPlay}>
              <BigPlayButton position="center" disabled />
              <ControlBar autoHide={true} className="controls-bar">
                <ClosedCaptionButton textTracks={this.state.player.textTracks} order={7} />
              </ControlBar>

              {type === "live" ? <HLSSource src={this.props.videoResource.source} /> : <source src={this.props.videoResource.source} />}

              {type === "live" && 
                <VideoDetails title={this.props.videoResource.channel_name} epg={this.props.videoResource.channel_epg.epg_listings} sourceType={this.props.videoResource.type} />
              }
              {type === "movie" && 
                <VodDetails details={this.props.videoResource.vod_info} />
              }
              
          </Player>
          <SourceBar player={this.state.player} toggleFullscreen={this.toggleFullscreen} play={this.play} pause={this.pause} />
        </div>
    )
     
  }
}


function mapStateToProps(state){
    return {
      videoResource : state.videoResource,
      sidebar : state.sidebar
    }
}

export default connect(mapStateToProps, null)(VideoPlayer);