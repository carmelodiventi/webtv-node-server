import React, { Component, createRef } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { gsap } from 'gsap';
import { getFullYear, getDuration, getSourceURL } from '../../utils/helpers';
import { getSeriesInfo, closeSerieInfo } from '../../actions/index';
import { withRouter } from 'react-router';
import InfoItem from '../../component/movies/InfoItem';
import Player from '../../component/vod/player/Player';
import Loader from '../../component/Loader';
import BackButton from '../../component/BackButton';
import Layout from '../../component/Layout';
import Seasons from './Seasons';


class SerieInfo extends Component {

    constructor(props) {
        super(props);
        this.animation = gsap.timeline({ paused: true })
        this.playerAnimation = gsap.timeline({ paused: true })
        this.player = createRef();
        this.title = createRef();
        this.info = createRef();
        this.plot = createRef();
        this.cover = createRef();
        this.seasons = createRef();
        this.backButton = createRef();
        this.state = {
            source: null,
            showPlayer: false
        }
    }

    componentDidMount() {

        const { series_id } = this.props.match.params;
        this.props.getSeriesInfo(series_id);

        this.animation
            .to(this.title,
                {
                    duration: .5,
                    opacity: .2
                }
            )
            .to([this.info, this.plot, this.cover, this.seasons, this.backButton],
                {
                    duration: 1,
                    y: 0,
                    opacity: 1,
                    stagger: .2
                })
            .play()

    }

    componentWillUnmount() {
        this.props.closeSerieInfo();
    }

    setSource = (stream_id, container_extension) => {
        const source = getSourceURL("series", stream_id, container_extension);
        this.setState({
            source
        }, () => {
            this.togglePlayer()
        })
    }

    togglePlayer = () => {
        this.setState({
            showPlayer: !this.state.showPlayer
        })
    }

    goBack = () => {
        this.animation.reverse().then(() => {
            this.props.history.goBack()
        });
    }

    render() {

        const { source, showPlayer } = this.state;
        const { series: { info: { info, episodes }, loading } } = this.props;
        const youtube_trailer = `https://youtu.be/${info?.youtube_trailer}` || null;

        return (
            <Layout>
                <div className={`vod-info`}>

                    <div className="section-title" ref={el => this.title = el}>
                        <span className="title">{info.name}</span>
                    </div>

                    <div className="description">
                        <div className="more-info">
                            <div className="info" ref={el => this.info = el}>
                                <ul>

                                    {info.genre &&
                                        <InfoItem label="Genre" text={info.genre} />
                                    }

                                    {info.cast &&
                                        <InfoItem label="Cast" text={info.cast} />
                                    }

                                    {info.country &&
                                        <InfoItem label="Country" text={info.country} />
                                    }

                                    {info.director &&
                                        <InfoItem label="Director" text={info.director} />
                                    }

                                    {info.duration_secs &&
                                        <InfoItem label="Duration" text={getDuration(info.duration_secs)} />
                                    }

                                    {info.releasedate &&
                                        <InfoItem label="Anno" text={getFullYear(info.releasedate)} />
                                    }

                                    {info.rating &&
                                        <InfoItem label="Rating" text={info.rating} />
                                    }
                                </ul>
                            </div>

                            <div className="plot" ref={el => this.plot = el}>
                                {info.name ? <span className="title">{info.name}</span> : <span className="title"> Title not found </span>}
                                {info.plot &&
                                    <p className="plot-text">{info.plot}</p>
                                }
                            </div>
                        </div>

                        <div className="cover" ref={el => this.cover = el}>
                            {info.cover &&
                                <img src={info.cover} alt="" />
                            }
                            <div className="btn-group">
                                {youtube_trailer &&
                                    <a href={youtube_trailer} className="btn btn-warning btn-outline btn-block" target="_new">Watch Trailer</a>
                                }
                            </div>
                        </div>
                    </div>

                    <Seasons episodes={episodes} setSource={this.setSource} ref={el => this.seasons = el} />
                    <Player source={source} showing={showPlayer} togglePlayer={this.togglePlayer} />
                    <BackButton goBack={this.goBack} ref={el => this.backButton = el} />
                    <Loader animating={loading} />

                </div>
            </Layout>
        );

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getSeriesInfo, closeSerieInfo }, dispatch);
}

function mapStateToProps(state) {
    return {
        series: state.series
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SerieInfo));
