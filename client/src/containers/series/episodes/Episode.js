import React, { useState, useEffect } from 'react';
import PlayButton from '../../../component/vod/player/PlayButton';

const Episode = ({ id, title, episode_num, container_extension, info: { movie_image }, setSource }) => {

    const [isHover, setIsHover] = useState(false);

    useEffect(() => {

    }, [isHover])

    const _onMouseHover = () => {
        setIsHover(true)
    }

    const _onMouseOut = () => {
        setIsHover(false)
    }

    const _onSetSource = () => {
        setSource(id, container_extension)
    }

    return (
        <div className="episode"
            onMouseOver={_onMouseHover}
            onMouseOut={_onMouseOut}
            onClick={_onSetSource}
            style={{ backgroundImage: `url(${movie_image})` }}>
            <PlayButton hover={isHover} />
            <span className="episode-num"> {title || `Episode ${episode_num}`} </span>
        </div>
    )
}

export default Episode;