import React, { useState, forwardRef, useEffect } from 'react';
import Episode from '../series/episodes/Episode';

const Seasons = forwardRef(({ episodes, setSource }, seasonsRef) => {

    const [activeSeason, setActiveSeason] = useState(0);
    const [seasons, setSeasons] = useState([]);
    
    

    useEffect(() => {
        setSeasons(getSeasons(episodes));
    }, [episodes, activeSeason])

    const setActiveSeasons = index => {
        setActiveSeason(index);
    }

    const getSeasons = episodes => {
        const episodeslist = [];
        let number = 1;
        for (let episode in episodes) {
            episodeslist.push({
                "number": number,
                'episodes': [
                    ...episodes[episode]
                ]
            })
            number++;
        }
        return episodeslist;
    }


    return (
        <div className="seasons" ref={seasonsRef}>

            <h3 className="seasons-title">
                Stagioni
            </h3>

            <div className="seasons-tab">

                <div className="seasons-tab__items">
                    {seasons.map((season, index) => (<button className={`seasons-tab__item ${activeSeason === index ? 'active' : ''}`} onClick={() => setActiveSeasons(index)} key={`season-${index}`}> Stagione {season.number} </button>))}
                </div>

                <div className="seasons-tab__content">

                    <div className={`seasons-tab__content--item active`}>
                        <div>
                            {seasons[activeSeason] && seasons[activeSeason].episodes &&

                                seasons[activeSeason].episodes.map(({ id, episode_num, title, container_extension, info }) => {
                                    return (
                                        <Episode id={id} title={title} episode_num={episode_num} container_extension={container_extension} info={info} setSource={setSource} key={id} />
                                    )
                                })
                            }
                        </div>
                    </div>

                </div>

            </div>

        </div>
    );

});

export default Seasons;