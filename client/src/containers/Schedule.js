import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ScheduleItem from './epg/ScheduleItem';

import { closeSchedule,setReplaySource } from '../actions/index';

class Schedule extends Component {

  constructor(props) {
    super(props);
    
    this.closeSchedule = this.closeSchedule.bind(this);
  }

  closeSchedule = () => {
    this.props.closeSchedule();
  }


  render() {
        const stream_id = this.props.videoResource.stream_id;
        let lastid = null;
        let items = this.props.videoResource.channelSchedule.map((item, i) => {
          let today = item.today ? 'active' : '';
          let entry = item.data.map((element, j) => {
                let nowPlaying = (element.now_playing == 1) ? ' now_playing' : '';
                let id = element.id;
                 if(lastid == null || id >= lastid){
                    lastid = id;
                    return (
                        <div className={`epg-data${nowPlaying}`} key={j}>
                          <ScheduleItem streamId={stream_id} data={element} nowPlaying={nowPlaying}/>
                          {element.has_archive === 1 &&
                            <div className="replay">
                              <button onClick={ () => {this.props.setReplaySource(element)}}>
                                <img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDQyNi42NjcgNDI2LjY2NyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNDI2LjY2NyA0MjYuNjY3OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCI+CjxnPgoJPGc+CgkJPHBhdGggZD0iTTIxMy4zMzMsODUuMzMzVjBMMTA2LjY2NywxMDYuNjY3bDEwNi42NjcsMTA2LjY2N1YxMjhjNzAuNzIsMCwxMjgsNTcuMjgsMTI4LDEyOHMtNTcuMjgsMTI4LTEyOCwxMjhzLTEyOC01Ny4yOC0xMjgtMTI4ICAgIEg0Mi42NjdjMCw5NC4yOTMsNzYuMzczLDE3MC42NjcsMTcwLjY2NywxNzAuNjY3UzM4NCwzNTAuMjkzLDM4NCwyNTZTMzA3LjYyNyw4NS4zMzMsMjEzLjMzMyw4NS4zMzN6IiBmaWxsPSIjRkZGRkZGIi8+Cgk8L2c+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg=="/>
                              </button>
                            </div>
                          }
                        </div>
                      );
                  }

                });
            return (
                    <li key={i} className={`${today}`}>
                      <div className="day">{item.key}</div> 
                      <div className="itemInDay">{entry}</div>
                    </li>
            );
        });
      
      
      return (<div className={`tv-schedule ${this.props.videoResource.showSchedule ? 'active' : 'hidden'}`}>
               <div className="section-title">
                  <div>Tv Schedule <span>{this.props.videoResource.channel_name}</span></div>
                  <div>
                    <img className="close" onClick={this.closeSchedule} src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzJweCIgdmVyc2lvbj0iMS4xIiBoZWlnaHQ9IjMycHgiIHZpZXdCb3g9IjAgMCA2NCA2NCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNjQgNjQiPgogIDxnPgogICAgPHBhdGggZmlsbD0iI0ZGRkZGRiIgZD0iTTI4Ljk0MSwzMS43ODZMMC42MTMsNjAuMTE0Yy0wLjc4NywwLjc4Ny0wLjc4NywyLjA2MiwwLDIuODQ5YzAuMzkzLDAuMzk0LDAuOTA5LDAuNTksMS40MjQsMC41OSAgIGMwLjUxNiwwLDEuMDMxLTAuMTk2LDEuNDI0LTAuNTlsMjguNTQxLTI4LjU0MWwyOC41NDEsMjguNTQxYzAuMzk0LDAuMzk0LDAuOTA5LDAuNTksMS40MjQsMC41OWMwLjUxNSwwLDEuMDMxLTAuMTk2LDEuNDI0LTAuNTkgICBjMC43ODctMC43ODcsMC43ODctMi4wNjIsMC0yLjg0OUwzNS4wNjQsMzEuNzg2TDYzLjQxLDMuNDM4YzAuNzg3LTAuNzg3LDAuNzg3LTIuMDYyLDAtMi44NDljLTAuNzg3LTAuNzg2LTIuMDYyLTAuNzg2LTIuODQ4LDAgICBMMzIuMDAzLDI5LjE1TDMuNDQxLDAuNTljLTAuNzg3LTAuNzg2LTIuMDYxLTAuNzg2LTIuODQ4LDBjLTAuNzg3LDAuNzg3LTAuNzg3LDIuMDYyLDAsMi44NDlMMjguOTQxLDMxLjc4NnoiLz4KICA8L2c+Cjwvc3ZnPgo=" />
                  </div>
               </div>
               <ul>
                {items}
               </ul>
            </div>
       );
    }
}


function mapDispatchToProps(dispatch){
  return bindActionCreators({closeSchedule,setReplaySource},dispatch)
}


function mapStateToProps(state){
    return {
      videoResource : state.videoResource,
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Schedule);