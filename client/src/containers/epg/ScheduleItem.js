import React, { Component } from 'react';
import {getTime,getStartEndTime,getNow,getCatchUpSourceURL} from '../../utils/helpers';
import base64 from 'base-64'; 

export default class ScheduleItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
        showDescription:false
    };
  }


  toggleDescription = () => {
    this.setState({
        showDescription: !this.state.showDescription
    })
  }

  onFocus = (e) => {
    const target = e.target;
    target.classList.add('focus');
  } 

  onUnfocus = (e) => {
    const target = e.target;
    target.classList.remove('focus');
  }

  onEnter = (e) => {
    const target = e.target;
    target.childNodes[0].click();
  }


  render() {
    return (
      <div>
       <div className='epg-content' onClick={this.toggleDescription}>
          {this.props.data.start_timestamp &&
              <time>
                {getTime(this.props.data.start_timestamp)}
              </time>
          }
          <div className="description">
              {this.props.data.title &&
                <h3 className="title">
                { base64.decode(this.props.data.title)} 
                </h3>
              }
              {this.props.data.description && this.state.showDescription &&
                 <p>{base64.decode(this.props.data.description)}</p>
              }
           </div>
       </div>
      </div>
    );
  }
}