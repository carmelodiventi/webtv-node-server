import React, { Component, createRef } from 'react';
import { withRouter } from 'react-router';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { gsap } from 'gsap';
import { bindActionCreators } from 'redux';
import { getLiveStreams } from '../../actions';
import Layout from '../../component/Layout';
import Loader from '../../component/Loader';
import Search from '../../component/search/Search';
import LivePlayer from '../../component/live/LivePlayer';
import Section from '../../component/Section';
import SectionSide from '../../component/SectionSide';
import BackButton from '../../component/BackButton';
import CategoriesItemLive from '../../component/categories/CategoriesItemLive';
import ApiXtreamCodes from '../../lib/xtream-codes';
import { getSourceURL, getCatchUpSourceURL } from '../../utils/helpers';


class LiveCategory extends Component {

	constructor(props) {
		super(props);
		this.state = {
			items: [],
			initialItems: [],
			source: null,
			stream_id: null,
			stream_name: '',
			tv_archive: 0,
			epg: { epg_listings: [] },
			channelSchedule: { epg_listings: [] },
			nowPlaying: null,
			showingSchedule: false
		}
		this.animation = gsap.timeline({ paused: true });
		this.side = createRef();
		this.playerRef = createRef();
		this.backButton = createRef();
	}

	componentDidMount() {
		const { category_id } = this.props.match.params;
		this.props.getLiveStreams(category_id);

		const videoWidth = this.playerRef.current.clientWidth;
		const videoHeight = videoWidth * 0.5625;
		this.playerRef.current.style.height = `${videoHeight}px`;

		this.animation
			.to(this.side.current,
				{
					duration: 1,
					x: 0,
					opacity: 1
				})
			.to([this.backButton],
				{
					duration: 1,
					y: 0,
					opacity: 1,
					stagger: .2
				})
			.play()

	}

	componentDidUpdate(prevProps) {
		if (this.props.live.items !== prevProps.live.items) {
			this.setState({
				items: this.props.live.items,
				initialItems: this.props.live.items
			})
		}
	}

	filterBy = e => {

		let currentItems = this.state.items;
		let filteredItems = [];

		if (e.target.value !== '') {

			currentItems = this.state.initialItems;

			filteredItems = currentItems.filter(item => {
				let name = item.name.toLowerCase();
				let filter = e.target.value.toLowerCase();
				return name.includes(filter);
			})

			this.setState({
				items: filteredItems
			})

		}

		else {

			this.setState({
				items: this.state.initialItems
			})

		}


	}

	mapItems = items => {
		return items.map(item => ({ ...item, id: item.stream_id, name: item.name, type: item.stream_type, icon: item.stream_icon }))
	}

	goBack = () => {
		this.animation.reverse().then(() => {
			this.props.history.goBack()
		});
	}

	setPlayerSource = async (stream_name, stream_type, stream_id, tv_archive) => {

		const { username, password } = JSON.parse(localStorage.getItem('user'));
		const api = new ApiXtreamCodes({ baseUrl: process.env.REACT_APP_API_URL, auth: { username, password } })
		const epg = await api.getEPGLiveStreams(stream_id, 1);
		const source = getSourceURL(stream_type, stream_id);
		this.setState({
			source,
			stream_id,
			stream_name,
			tv_archive,
			epg,
			nowPlaying : stream_id,
			showingSchedule: false
		})
	}

	setReplaySource = ({ start, end, id, ...replay }) => {
		const { stream_id } = this.state;
		const source = getCatchUpSourceURL(start, end, stream_id);
		console.log(source)
		const epg = {
			epg_listings : [replay]
		}
		this.setState({
			source,
			epg,
			nowPlaying : stream_id,
			showingSchedule: false
		})
	}

	getAllEPGLiveStreams = async (stream_id) => {

		const { username, password } = JSON.parse(localStorage.getItem('user'));
		const api = new ApiXtreamCodes({ baseUrl: process.env.REACT_APP_API_URL, auth: { username, password } })
		const channelSchedule = await api.getAllEPGLiveStreams(stream_id);

		this.setState({
			channelSchedule
		}, this.toggleChannelSchedule())
	}

	toggleChannelSchedule = () => {
		this.setState({
			showingSchedule: !this.state.showingSchedule
		})
	}


	render() {

		const { source, stream_name, stream_id, tv_archive, epg, channelSchedule, showingSchedule } = this.state;

		return (
			<Layout>
				<div className="live">
					<div className="section">

						<Section>

							<LivePlayer
								source={source}
								streamName={stream_name}
								streamID={stream_id}
								tvArchive={tv_archive}
								epg={epg}
								channelSchedule={channelSchedule}
								showingSchedule={showingSchedule}
								toggleChannelSchedule={this.toggleChannelSchedule}
								getAllEPGLiveStreams={this.getAllEPGLiveStreams}
								setReplaySource={this.setReplaySource}
								ref={{
									playerRef: this.playerRef
								}}
							/>

						</Section>

						<SectionSide ref={this.side}>
							<Search filterBy={this.filterBy} />
							<ul className="categories">
								{this.mapItems(this.state.items).map(({ id, name, type, icon, tv_archive }) => <CategoriesItemLive name={name} type={type} icon={icon} id={id} nowPlaying={this.state.nowPlaying} tvArchive={tv_archive} onClick={this.setPlayerSource} key={id} />)}
							</ul>
						</SectionSide>

					</div>
				</div>
				<BackButton goBack={this.goBack} ref={el => this.backButton = el} />
				<Loader animating={this.props.live.loading} />
			</Layout>
		);
	}
}

function mapStateToProps(state) {
	return {
		live: state.live
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({ getLiveStreams }, dispatch);
}

export default compose(
	withRouter,
	connect(mapStateToProps, mapDispatchToProps)
)(LiveCategory);