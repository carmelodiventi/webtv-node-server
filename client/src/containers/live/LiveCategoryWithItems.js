import React, { Component, createRef } from 'react';
import { withRouter } from 'react-router';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { gsap } from 'gsap';
import { bindActionCreators } from 'redux';
import { getLiveStreams } from '../../actions';
import Layout from '../../component/Layout';
import Loader from '../../component/Loader';
import Search from '../../component/search/Search';
import BackButton from '../../component/BackButton';
import ApiXtreamCodes from '../../lib/xtream-codes';
import LiveItem from '../../component/live/LiveItem';


class LiveCategory extends Component {

	constructor(props) {
		super(props);
		this.state = {
			items: [],
			initialItems: [],
			source: null,
			nowPlaying: null,
			epg: { epg_listings: [] }
		}
		this.animation = gsap.timeline({ paused: true })
		this.backButton = createRef();
	}

	componentDidMount() {
		const { category_id } = this.props.match.params;
		this.props.getLiveStreams(category_id);

		this.animation
			.to([this.backButton],
				{
					duration: 1,
					y: 0,
					opacity: 1,
					stagger: .2
				})
			.play()

	}

	componentDidUpdate(prevProps) {
		if (this.props.live.items !== prevProps.live.items) {
			this.setState({
				items: this.props.live.items,
				initialItems: this.props.live.items
			})
		}
	}

	filterBy = e => {

		let currentItems = this.state.items;
		let filteredItems = [];

		if (e.target.value !== '') {

			currentItems = this.state.initialItems;

			filteredItems = currentItems.filter(item => {
				let name = item.name.toLowerCase();
				let filter = e.target.value.toLowerCase();
				return name.includes(filter);
			})

			this.setState({
				items: filteredItems
			})

		}

		else {

			this.setState({
				items: this.state.initialItems
			})

		}


	}

	mapItems = items => {
		return items.map(item => ({ ...item, id: item.stream_id, name: item.name, type: item.stream_type, icon: item.stream_icon }))
	}

	goBack = () => {
		this.animation.reverse().then(() => {
			this.props.history.goBack()
		});
	}

	setPlayerSource = async (source, stream_id) => {

		const { username, password } = JSON.parse(localStorage.getItem('user'));
		const api = new ApiXtreamCodes({ baseUrl: process.env.REACT_APP_API_URL, auth: { username, password } })
		const epg = await api.getEPGLiveStreams(stream_id, 1);

		this.setState({
			source,
			nowPlaying: stream_id,
			epg
		})
	}


	render() {

		const { source, epg } = this.state;

		return (
			<Layout>
				<Search filterBy={this.filterBy} />
				<div className="live-content">
					{this.mapItems(this.state.items).map(({ id, name, type, icon }) => <LiveItem name={name} stream_type={type} stream_icon={icon} stream_id={id} nowPlaying={this.state.nowPlaying} key={id} onClick={this.setPlayerSource} />)}
				</div>
				<BackButton goBack={this.goBack} ref={el => this.backButton = el} />
				<Loader animating={this.props.live.loading} />
			</Layout>
		);
	}
}

function mapStateToProps(state) {
	return {
		live: state.live
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({ getLiveStreams }, dispatch);
}

export default compose(
	withRouter,
	connect(mapStateToProps, mapDispatchToProps)
)(LiveCategory);