import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import { withToastManager } from 'react-toast-notifications';
import { getFeatured } from '../actions/index';
import { getFeaturedCategory } from '../utils/auth'
import Search from '../component/search/Search';
import Layout from '../component/Layout';
import Loader from '../component/Loader';
import MoviesList from '../component/movies/MoviesList';
import { Link } from 'react-router-dom';


class Featured extends Component {

	constructor(props) {
		super(props);
		this.state = {
			items: [],
			initialItems: [],
			featuredCategory: getFeaturedCategory()
		}
	}

	componentDidMount() {
		const { toastManager } = this.props;
		this.props.getFeatured().then(res => {
			if (res.error) {
				toastManager.add(res.error.message, {
					appearance: 'error'
				})
			}
		});
	}

	componentDidUpdate(prevProps) {
		if (this.props.featured.items !== prevProps.featured.items) {
			this.setState({
				items: this.props.featured.items,
				initialItems: this.props.featured.items
			})
		}
	}

	mapItems = items => {
        return items.map(item => ({ ...item, id: item.category_id, name: item.category_name }))
    }

	filterBy = e => {

		let currentItems = this.state.items;
		let filteredItems = [];

		if (e.target.value !== '') {

			currentItems = this.state.initialItems;

			filteredItems = currentItems.filter(item => {
				let name = item.name.toLowerCase();
				let filter = e.target.value.toLowerCase();
				return name.includes(filter);
			})

			this.setState({
				items: filteredItems
			})

		}

		else {

			this.setState({
				items: this.state.initialItems
			})

		}


	}


	render() {

		return (
			<Layout>

				{!this.state.featuredCategory &&
					<div className="movies">
						<div className="message">
							Seams that you have not set up any prefered category to show on your home
							<Link to="/account"> go to my account's page </Link>
						</div>
					</div>
				}

				{this.state.items.length > 0 &&
					<div className="featured">
						<Search filterBy={this.filterBy} />
						<MoviesList data={this.state.items} />
					</div>
				}

				<Loader animating={this.props.featured.loading} />
			</Layout>
		)
	}

}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({ getFeatured }, dispatch);
}

function mapStateToProps(state) {
	return {
		featured: state.featured
	}
}


export default compose(
	connect(mapStateToProps, mapDispatchToProps),
	withToastManager
)(Featured);