import React, { Component, createRef } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import { withToastManager } from 'react-toast-notifications';
import { gsap } from 'gsap';
import { getAccountInfo } from '../actions'
import Layout from '../component/Layout';
import Section from '../component/Section';
import AccountData from '../component/AccountData';
import Loader from '../component/Loader';
import SectionSide from '../component/SectionSide';


class Account extends Component {

    constructor(props) {
        super(props);
        this.animation = gsap.timeline({ paused: true });
        this.section = createRef();
        this.side = createRef();
    }


    componentDidMount() {

        const { toastManager } = this.props;
        this.props.getAccountInfo().then(res => {
            if (res.error) {
                toastManager.add(res.error.message, {
                    appearance: 'error'
                })
            }
        });

        this.animation
            .to(this.section.current,
                {
                    duration: 1,
                    y: 0,
                    opacity: 1
                })
            .to(this.side.current,
                {
                    duration: 1,
                    x: 0,
                    opacity: 1
                })
            .play()


    }

    render() {

        const { username, active_cons, exp_date, status, auth, allowed_output_formats } = this.props.auth.user;

        return (
            <Layout>
                <div className="account">
                    <div className="section">
                        <Section text="Your Account" ref={this.section} />
                        <SectionSide ref={this.side}>
                            <AccountData
                                username={username}
                                active_cons={active_cons}
                                exp_date={exp_date}
                                status={status}
                                auth={auth}
                                allowed_output_formats={allowed_output_formats}
                            />
                        </SectionSide>
                    </div>
                </div>
                <Loader animating={this.props.auth.loading} />
            </Layout>
        );
    }
}


function mapStateToProps(state) {
    return {
        auth: state.authentication
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getAccountInfo }, dispatch);
}


export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    withToastManager
)(Account);