import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import Select from 'react-select';
import { getFeaturedStreamCategories } from '../../actions/index';


class FeaturedSelect extends Component {

    state = {
        selectedOption: null,
    }

    componentDidMount() {
        this.props.getFeaturedStreamCategories()
    }

    handleChange = (selectedOption) => {
        this.setState({ selectedOption });
        const { value } = selectedOption;
        localStorage.setItem("featuredCategory", JSON.stringify({ id: value }))
    }

    mapOptions = item => {
        return { value: item.category_id, label: item.category_name.toLowerCase() }
    }

    render() {
        return (
            <Select
                className="react-select"
                value={this.state.selectedOption}
                onChange={this.handleChange}
                options={this.props.featured.categories.map(item => this.mapOptions(item))}
            />
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getFeaturedStreamCategories }, dispatch);
}

function mapStateToProps(state) {
    return {
        featured: state.featured
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FeaturedSelect);