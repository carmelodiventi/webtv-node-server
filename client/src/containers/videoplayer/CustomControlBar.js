import React from 'react';
import { withRouter } from 'react-router-dom';
import VideoInfo from './customcontrolbar/VideoInfo';
import Flexbox from 'flexbox-react';
import { IoIosPause, IoIosMenu, IoIosPlay, IoIosArrowBack } from 'react-icons/io';
import Duration from './customcontrolbar/Durations';
import ProgressBar from './customcontrolbar/ProgessBar';

let timeoutControlBar;

class CustomControlBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userIsInActivity: true,
            userIsChangingSource: false,
        }
    }

    componentDidMount() {
        document.addEventListener('keydown', this.resetTimer);
        //document.addEventListener('keyup', (e)=>{ console.log(e.keyCode) });
        this.startTimer();
    }


    componentWillUnmount() {
        clearTimeout(timeoutControlBar);
        document.removeEventListener('keydown', this.resetTimer);
    }

    startTimer = () => {
        timeoutControlBar = setTimeout(this.goInactive, 3000);
    }

    resetTimer = e => {
        clearTimeout(timeoutControlBar);
        this.goActive();
    }

    goInactive = () => {
        // do something
        if (!this.state.userIsChangingSource) {
            this.setState({
                userIsInActivity: false
            });
        }
    }

    goActive = () => {
        // do something
        this.setState({
            userIsInActivity: true
        });
        this.startTimer();
    }

    handleChangeSource = () => {
        this.setState({
            userIsChangingSource: !this.state.userIsChangingSource
        })
    }

    goBack = () => {
        new Promise((resolve) => {
            resolve(this.props.onClose())
        }).then(() => {
            this.props.history.goBack()
        });
    }


    render() {

        const { playing, duration, played, seeking, loaded, onSeekChange, onSeekMouseDown, onSeekMouseUp, playPause, auth, videoResource, handleVideoSource, categories } = this.props;
        const { userIsInActivity, userIsChangingSource } = this.state;

        return (
            <div className={`custom-controls-bar ${userIsInActivity ? 'user-active' : 'user-inactive'} ${userIsChangingSource ? 'user-ischanging' : 'user-iswatching'}`}>

                <Flexbox className="top-bar">
                    <div className="video-actions-button">

                        <button className="video-button video-button-go-back" alt="Go Back" type="button" onClick={this.goBack}>
                            <IoIosArrowBack />
                        </button>

                    </div>
                </Flexbox>

                <Flexbox className="center-bar">
                    <div className="video-info-details">
                        <VideoInfo videoResource={videoResource} />
                    </div>
                </Flexbox>

                <Flexbox className="bottom-bar">
                    <div className="video-actions-button">


                        <button className="video-button video-button-menu" type="button" alt="Menu" onClick={this.handleChangeSource}>
                            <IoIosMenu />
                        </button>


                        {!playing &&

                            <button className="video-button video-button-play" alt="Play" type="button" onClick={playPause}>
                                <IoIosPlay />
                            </button>

                        }
                        {playing &&
                            <>
                                <button className="video-button video-button-pause" alt="Pause" type="button" onClick={playPause}>
                                    <IoIosPause />
                                </button>
                            </>
                        }

                        <div className="video-info-status">
                            <Duration seconds={duration * played} />
                            <ProgressBar className='status-bar' seconds={duration * played} seeking={seeking} played={played} duration={duration} loaded={loaded} onSeekChange={onSeekChange} onSeekMouseDown={onSeekMouseDown} onSeekMouseUp={onSeekMouseUp} />
                            <Duration seconds={duration} />
                        </div>
                    </div>
                </Flexbox>
            </div >
        )
    }

}


export default withRouter(CustomControlBar);