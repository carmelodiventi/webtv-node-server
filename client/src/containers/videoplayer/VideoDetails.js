import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import base64 from 'base-64'; 
import {getTime,getStartEndTime,getNow} from '../../utils/helpers';

const propTypes = {
  player: PropTypes.object,
  className: PropTypes.string,
};

export default class VideoDetails extends Component {

  constructor(props, context) {
    super(props, context);
  }


  channelInfo = () => {
        let channel_epg;
        if(this.props.epg && this.props.epg.length > 0){
        channel_epg = [];
        this.props.epg.map((item,index) =>{
             channel_epg.push(
              <div key={index}>
                    <div className="title">
                      {item.title &&
                        <span>{ base64.decode(item.title)}</span>
                      }
                      <span className="channel-name">{this.props.title}</span>

                      {item.start && item.end && 
                        <time>{getStartEndTime(item.start)} - {getStartEndTime(item.end)}</time>
                      }
                      
                    </div>

                     {item.description &&
                         <div className="description">
                           <p>
                            {base64.decode(item.description)}
                           </p>
                        </div>
                    }
              </div>
            );
        });

        return channel_epg;

        }
    }

  render() {
    const { player, className } = this.props;
  
    return (
      <div className="video-react-description video-react-description-auto-hide">
        
        {this.props.sourceType == 'live' && 
          <div className="channel_info">

            {this.channelInfo()}
          </div>
        }
      </div>
    );
    
  }
}

VideoDetails.propTypes = propTypes;
