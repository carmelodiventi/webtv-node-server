import React,{Component} from 'react';
import {Child} from "react-focus-navigation";

class CustomControlBarItem extends Component {

    constructor(props){
        super(props);
        this.state = { 
            focused: false 
        };
    }

   
    onFocus = () => {
        this.setState(() => {
            return { 
                focused: true,
            };
        });
    }
      
    onBlur = () => {
        this.setState(() => {
            return { focused: false };
        });
    }
      

    render(){
      
        return(
            <Child onFocus={() => this.onFocus()} onBlur={() => this.onBlur()} onEnter={() => this.props.onEnter()}>
                <li className={this.state.focused ? `${this.state.active ? 'active focusable focused' : 'focusable focused'}` : `${this.state.active ? 'active focusable' : 'focusable'}`}>
                    {this.props.children}
                </li>
            </Child>
        )
    }

}

export default CustomControlBarItem;