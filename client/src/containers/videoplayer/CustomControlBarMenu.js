import React, {Component} from 'react';
import CustomControlBarMenuItem from './CustomControlBarMenuItem';

let sideMenu;

export default class CustomControlBarMenu extends Component {


    componentDidMount(){
        sideMenu = document.getElementById('side-menu');
    }

    render(){
        //console.log(this.props.videoResources);
        return(
            <div className="categories-content" id="side-menu" 
              ref={content => { this.content = content; }}>
                <div className="sub-categories-list" 
                  withFocus={this.props.withFocus} 
                  style={{ overflow: "hidden", display: "block" }}>
                    {this.props.data.map( (item,index) => <CustomControlBarMenuItem item={item} key={index} onEnter={this.props.onEnter} auth={this.props.auth} stream_type={this.props.videoResource.stream_type} />)}
                </div>
            </div>
        )
    }

}