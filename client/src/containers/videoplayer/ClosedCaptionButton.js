import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classNames from 'classnames';
import icon from '../../img/if_cc_1608614.svg';

const propTypes = {
  player: PropTypes.object,
  className: PropTypes.string,
};


export default class ClosedCaptionButton extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      textTracks : false
    }
    this.handleClick = this.handleClick.bind(this);
  }


  componentDidUpdate(prevProps, prevState) {
      if (prevProps.textTracks && this.props.textTracks !== prevProps.textTracks) {
          if(prevProps.textTracks && prevProps.textTracks.length >= 1){
            this.setState({
              textTrack:true
            })
          }
      }
   }

  handleClick() {
    this.setState({
      textTracks : !this.state.textTracks
    })
  }

  setTextTrack(item){
    const { textTracks, className } = this.props;
    (textTracks[item].mode == 'hidden') ? textTracks[item].mode = 'showing' : textTracks[item].mode ='hidden';
  }

  render() {
    const { textTracks, className } = this.props;
    let textTracksList = [];
    if(textTracks && textTracks.length >= 1){
      const textTracksNum = textTracks.length;
      for(let i = 0; i < textTracksNum; i++){
        textTracksList.push(<li key={i} onClick={()=>this.setTextTrack(i)}>{textTracks[i].label}</li>);
      }
    }

    return (
    <div>
    {textTracksList.length > 0 &&
      <button
        ref={ (c) => { this.button = c; }}
        className={classNames(className, {
          'video-react-control': true,
          'video-react-button': true,
        })}
        style={{
          backgroundImage: `url(data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDM4NCAzODQiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDM4NCAzODQ7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik0zNDEuMzMzLDIxLjMzM0g0Mi42NjdDMTkuMDkzLDIxLjMzMywwLDQwLjQyNywwLDY0djI1NmMwLDIzLjU3MywxOS4wOTMsNDIuNjY3LDQyLjY2Nyw0Mi42NjdoMjk4LjY2NyAgICBDMzY0LjkwNywzNjIuNjY3LDM4NCwzNDMuNTczLDM4NCwzMjBWNjRDMzg0LDQwLjQyNywzNjQuOTA3LDIxLjMzMywzNDEuMzMzLDIxLjMzM3ogTTE3MC42NjcsMTcwLjY2N2gtMzJWMTYwSDk2djY0aDQyLjY2NyAgICB2LTEwLjY2N2gzMnYyMS4zMzNjMCwxMS43MzMtOS40OTMsMjEuMzMzLTIxLjMzMywyMS4zMzNoLTY0QzczLjQ5MywyNTYsNjQsMjQ2LjQsNjQsMjM0LjY2N3YtODUuMzMzICAgIEM2NCwxMzcuNiw3My40OTMsMTI4LDg1LjMzMywxMjhoNjRjMTEuODQsMCwyMS4zMzMsOS42LDIxLjMzMywyMS4zMzNWMTcwLjY2N3ogTTMyMCwxNzAuNjY3aC0zMlYxNjBoLTQyLjY2N3Y2NEgyODh2LTEwLjY2N2gzMiAgICB2MjEuMzMzQzMyMCwyNDYuNCwzMTAuNTA3LDI1NiwyOTguNjY3LDI1NmgtNjRjLTExLjg0LDAtMjEuMzMzLTkuNi0yMS4zMzMtMjEuMzMzdi04NS4zMzNjMC0xMS43MzMsOS40OTMtMjEuMzMzLDIxLjMzMy0yMS4zMzMgICAgaDY0YzExLjg0LDAsMjEuMzMzLDkuNiwyMS4zMzMsMjEuMzMzVjE3MC42Njd6IiBmaWxsPSIjRkZGRkZGIi8+Cgk8L2c+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==)`,
          backgroundRepeat: 'no-repeat',
          backgroundPosition: 'center',
          backgroundSize: '50%'
        }}
        tabIndex="0" onClick={this.handleClick}>
        {this.state.textTracks &&
        <ul className="cc">
            {textTracksList}
          </ul>
        }
        </button>
      }
      </div>
    );
  }
}

ClosedCaptionButton.propTypes = propTypes;