import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { Focusable, FocusableSection } from 'react-js-spatial-navigation';
import { getTime, getStartEndTime, getNow, getDuration, getFullYear } from '../utils/helpers';
import base64 from 'base-64';

import { getSchedule } from '../actions/index';


class SourceBar extends Component {

	constructor(props) {
		super(props);

		this.state = {
			getInfo: false,
			getSchedule: false,
			showDescription: false,
		}

		this.getInfo = this.getInfo.bind(this);
		this.toggleFullscreen = this.toggleFullscreen.bind(this);
		this.play = this.play.bind(this);
		this.pause = this.pause.bind(this);
	}


	getInfo() {
		this.setState({
			getInfo: !this.state.getInfo
		})
	}

	toggleFullscreen() {
		this.props.toggleFullscreen();
	}

	play() {
		this.props.play();
	}

	pause() {
		this.props.pause();
	}



	render() {

		let items = [];
		let epg_listings;
		let styles = {};

		let disabled = this.props.player.hasStarted ? '' : 'disabled';
		let hasStarted = this.props.player.hasStarted ? true : false;
		let paused = this.props.player.paused ? true : false;
		let currentTime = this.props.player.currentTime;
		let waiting = this.props.player.waiting

		if (this.props.videoResource !== undefined && this.props.videoResource.channel_epg) {

			epg_listings = this.props.videoResource.channel_epg.epg_listings;

			if (epg_listings) {
				epg_listings.map((item, index) => {
					items.push(
						<div key={index} className="live">
							<div className="channel_info">
								<div className="title">
									<small>{base64.decode(item.title)}</small>
									{item.title &&
										<small className="channel-name">{this.props.videoResource.channel_name}</small>
									}
									{item.start && item.end &&
										<time>{getStartEndTime(item.start)} - {getStartEndTime(item.end)}</time>
									}
									{this.state.getInfo && item.description &&
										<p className="description">{base64.decode(item.description)}</p>
									}
								</div>
							</div>

							<div className="menu-options">
								<div className="video-actions-button-bottom">
									{item.description &&
										<Focusable onFocus={this.onFocus} onUnfocus={this.onUnfocus} onClickEnter={this.onEnter}>
											<button
												className="video-button video-button-info"
												onClick={() => this.getInfo()}
												alt="Info">
											</button>
										</Focusable>
									}
									<Focusable onFocus={this.onFocus} onUnfocus={this.onUnfocus} onClickEnter={this.onEnter}>
										<button className="video-button video-button-schedule"
											onClick={() => this.props.getSchedule(this.props.videoResource.stream_id)}
											alt="Channel Schedule">
										</button>
									</Focusable>
									{paused && hasStarted &&
										<Focusable onFocus={this.onFocus} onUnfocus={this.onUnfocus} onClickEnter={this.onEnter}>
											<button
												className="video-button video-button-play"
												alt="Play"
												type="button" onClick={this.play}>
											</button>
										</Focusable>
									}
									{hasStarted && currentTime > 0 &&
										<Focusable onFocus={this.onFocus} onUnfocus={this.onUnfocus} onClickEnter={this.onEnter}>
											<button
												className="video-button video-button-fullscreen"
												alt="Fullscreen"
												type="button" onClick={this.toggleFullscreen}>
											</button>
										</Focusable>
									}

									<Focusable onFocus={this.onFocus} onUnfocus={this.onUnfocus} onClickEnter={this.onEnter}>
										<button
											className="video-button video-button-go-back"
											alt="Show Sidebar"
											type="button" onClick={() => this.props.showSidebar()}>
										</button>
									</Focusable>

								</div>
							</div>
						</div>
					)
				});
			}

		}

		if (this.props.videoResource !== undefined && this.props.videoResource.channel_epg.info) {
			let item = this.props.videoResource.channel_epg.info;

			let is_cover = null;

			let imgUrl = null;
			let youtube_trailer = null;

			let cover = item.cover_big || item.movie_image;

			styles.overflow = 'auto';
			styles.backgroundImage = null;

			if (item.youtube_trailer) {
				youtube_trailer = `https://youtu.be/${item.youtube_trailer}`;
			}

			if (item.backdrop_path && item.backdrop_path.length > 0) {
				imgUrl = item.backdrop_path[0];
				styles.backgroundImage = `url(${imgUrl})`;
				styles.backgroundColor = 'transparent';
			}

			if (this.props.player.hasStarted && this.props.player.currentTime > 0) {
				if (styles.backgroundImage && styles.backgroundImage.length > 0) {
					styles.backgroundImage = null
				}
			}

			is_cover = !imgUrl ? 'is-cover' : '';

			items.push(
				<div key={item.name} className="movie">

					<div className="section-title">
						<div>
							Vod Info

			                        {waiting && currentTime == 0 &&
								<span className="video-status">Loading...</span>
							}

							{currentTime > 0 && !paused &&
								<span className="video-status">In Playing...</span>
							}

							{paused && hasStarted &&
								<span className="video-status">In Pause...</span>
							}

						</div>
						<Focusable onFocus={this.onFocus} onUnfocus={this.onUnfocus} onClickEnter={this.onEnter}>
							<img className="close" onClick={() => this.props.showSidebar()} src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzJweCIgdmVyc2lvbj0iMS4xIiBoZWlnaHQ9IjMycHgiIHZpZXdCb3g9IjAgMCA2NCA2NCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNjQgNjQiPgogIDxnPgogICAgPHBhdGggZmlsbD0iI0ZGRkZGRiIgZD0iTTI4Ljk0MSwzMS43ODZMMC42MTMsNjAuMTE0Yy0wLjc4NywwLjc4Ny0wLjc4NywyLjA2MiwwLDIuODQ5YzAuMzkzLDAuMzk0LDAuOTA5LDAuNTksMS40MjQsMC41OSAgIGMwLjUxNiwwLDEuMDMxLTAuMTk2LDEuNDI0LTAuNTlsMjguNTQxLTI4LjU0MWwyOC41NDEsMjguNTQxYzAuMzk0LDAuMzk0LDAuOTA5LDAuNTksMS40MjQsMC41OWMwLjUxNSwwLDEuMDMxLTAuMTk2LDEuNDI0LTAuNTkgICBjMC43ODctMC43ODcsMC43ODctMi4wNjIsMC0yLjg0OUwzNS4wNjQsMzEuNzg2TDYzLjQxLDMuNDM4YzAuNzg3LTAuNzg3LDAuNzg3LTIuMDYyLDAtMi44NDljLTAuNzg3LTAuNzg2LTIuMDYyLTAuNzg2LTIuODQ4LDAgICBMMzIuMDAzLDI5LjE1TDMuNDQxLDAuNTljLTAuNzg3LTAuNzg2LTIuMDYxLTAuNzg2LTIuODQ4LDBjLTAuNzg3LDAuNzg3LTAuNzg3LDIuMDYyLDAsMi44NDlMMjguOTQxLDMxLjc4NnoiLz4KICA8L2c+Cjwvc3ZnPgo=" />
						</Focusable>
					</div>


					<div className="channel_info">
						<div className="title">
							{item.name &&
								<small className="channel-name">{item.name}</small>
							}
							{!item.name &&
								<small className="channel-name">{this.props.videoResource.channel_name}</small>
							}
							<div className="av-badges">

								{item.genre &&
									<span className="av-badge-text">
										{item.genre}
									</span>
								}
								{item.duration_secs &&
									<span className="av-badge-text">
										{getDuration(item.duration_secs)}
									</span>
								}
								{item.releasedate &&
									<span className="av-badge-text">
										{getFullYear(item.releasedate)}
									</span>
								}

								{item.rating &&
									<span className="av-badge-text">
										<span className="av-imdb-logo av-icon av-icon--logo_imdb">
											<span className="avu-screen-reader-only">IMDb</span>
										</span>
										&nbsp;
						                      {item.rating}
									</span>
								}
							</div>
							<div className="video-options">
								<FocusableSection defaultElement="video-button-play">
									{!hasStarted &&
										<Focusable onFocus={this.onFocus} onUnfocus={this.onUnfocus} onClickEnter={this.onEnter}>
											<button className="btn btn-primary video-button-play" onClick={() => this.play()} alt="Play Movie">
												Play Movie
					                        </button>
										</Focusable>
									}
								</FocusableSection>

								{waiting && currentTime == 0 &&
									<span className="video-status">Loading...</span>
								}

								{currentTime > 0 && !paused &&
									<span className="video-status">In Playing...</span>
								}

								{paused && hasStarted &&
									<span className="video-status">In Pause...</span>
								}

							</div>
							<div className="description">
								{item.movie_image && item.cover_big == undefined && !imgUrl &&
									<img src={item.movie_image} className="cover" />
								}
								{item.cover_big && !imgUrl &&
									<img src={item.cover_big} className="cover" />
								}
								{item.plot &&
									<p>{item.plot}</p>
								}

								{item.cast &&
									<p><strong className="evidenced">Cast:</strong> {item.cast} </p>
								}
								{item.genre &&
									<p><strong className="evidenced">Genre:</strong> {item.genre}</p>
								}

								{item.country &&
									<p><strong className="evidenced">Country:</strong> {item.country}</p>
								}
								{item.director &&
									<p><strong className="evidenced">Director:</strong> {item.director}</p>
								}

							</div>
						</div>
					</div>
					<div className="menu-options">
						<div className="video-actions-button-bottom">

							{paused && hasStarted &&
								<Focusable onFocus={this.onFocus} onUnfocus={this.onUnfocus} onClickEnter={this.onEnter}>
									<button
										className="video-button video-button-play"
										alt="Play"
										type="button" onClick={this.play}>
									</button>
								</Focusable>
							}
							{hasStarted && currentTime > 0 && !paused &&
								<Focusable ref="SpatialNavigation" onFocus={this.onFocus} onUnfocus={this.onUnfocus} onClickEnter={this.onEnter}>
									<button
										className="video-button video-button-pause"
										alt="Pause"
										type="button" onClick={this.pause}>
									</button>
								</Focusable>
							}
							{hasStarted && currentTime > 0 &&
								<Focusable onFocus={this.onFocus} onUnfocus={this.onUnfocus} onClickEnter={this.onEnter}>
									<button
										className="video-button video-button-fullscreen"
										alt="Fullscreen"
										type="button" onClick={this.toggleFullscreen}>
									</button>
								</Focusable>
							}

						</div>
					</div>
				</div>
			)
		}

		if (this.props.videoResource.channel_name !== null && this.props.videoResource.source !== null) {
			return (
				<div className="source-bar" style={styles}>
					{items}
				</div>
			)
		}
		else {
			return null
		}
	}


}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({ getSchedule }, dispatch);
}


function mapStateToProps(state) {
	return {
		videoResource: state.videoResource,
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(SourceBar);