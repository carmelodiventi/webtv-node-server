import React from 'react';


const VodDetails = props => {

	return (
		<div className="video-description video-description-auto-hide">
			{props.details &&
				<div className="channel_info">
					<div> 
						
						<div className="title">
							<span>{props.details.info.genre && props.details.info.genre}</span>
							<span className="channel-name">
								{ props.details.movie_data && props.details.movie_data.name && props.details.movie_data.name }</span>
							<time>{props.details.info && props.details.info.duration}</time>
						</div>
						<div className="description">
							<p>{props.details.info && props.details.info.plot}</p>
						</div>
					</div>
				</div>
			}
		</div>
	)


}

export default VodDetails;