import React from 'react';
import { Child } from "react-focus-navigation";
import {getSourceURL} from '../../utils/helpers';
import propTypes from 'prop-types';

class CustomControlBarMenuItem extends React.Component{

    constructor(props){
        super(props);
        this.state = { 
            focused: false
        };
    }

    onFocus = () => {
        this.setState(() => {
            return { focused: true };
        });
    }
      
    onBlur = () => {
        this.setState(() => {
            return { focused: false };
        });
    }


    onEnter = () => {

        const {stream_type,stream_id,container_extention,name} = this.props.item;
        const {auth,category} = this.props;

        let resource = {
            source : getSourceURL(stream_type,stream_id,container_extention,auth),
            channel_name : name,
            category : category,
            stream_id : stream_id,
            stream_type : stream_type
        }
        this.props.onEnter(resource);
    }

    render(){

        return(
            <Child onFocus={() => this.onFocus()} onBlur={() => this.onBlur()} onEnter={() => this.onEnter()}>
                <li className={this.state.focused ? "focusable focused" : "focusable"}>
                    <button
                            className={'category-item'}
                            type='button'
                            title={this.props.item.name}
                            stream_type={this.props.item.stream_type}>
                                {this.props.item.name &&
                                    this.props.item.name.toLowerCase()
                                }
                        </button>
                </li>
            </Child>
        )
    }

} 

CustomControlBarMenuItem.propTypes = {
    type : propTypes.string,
    title : propTypes.string,
    source : propTypes.string
}

export default CustomControlBarMenuItem;