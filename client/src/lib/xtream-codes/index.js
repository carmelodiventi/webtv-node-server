import axios from 'axios';
import { stringify } from 'querystring';
import { pickBy } from 'lodash';


class ApiXtreamCodes {

    /**
       * @param {{ baseUrl: string, auth: { username: string, password: string } }} [config]
    */

    constructor(config = {}) {
        this.config = config
    }

    /**
    * @param {string} baseURL
    */

    static setBaseURL(baseURL) {
        if (!baseURL) {
            throw new Error('baseURL must be null')
        }

        this.baseUrl = baseURL
    }

    /**
    * @param {string} username
    * @param {string} password
    */

    static setAuth(username, password) {
        this.config.auth = { username, password }
    }


    /**
   * execute query on xtream server
   *
   * @param {string} [action]
   * @param {{ [ key: string ]: string }} [filter]
   * @returns {Promise<any>}
   */

    async execute(action, filter) {

        const query = pickBy({ ...this.config.auth, action, ...filter });
        const url = `${this.config.baseUrl}?${stringify(query)}`;


        try {

            const res = await axios.get(url);

            if (action && res.data.hasOwnProperty('user_info') &&
                res.data.user_info.hasOwnProperty('status') &&
                res.data.user_info.status === 'Disabled') {
                throw new Error('account disabled')
            }

            return res.data;


        } catch (err) {
            throw new Error(err)
        }



    }

    async getAccountInfo() {

        return this.execute().then(res => {
            if (res.user_info.auth === 0) {
                throw new Error('authentication error')
            }
            return res
        })
    }

    getLiveStreamCategories() {
        return this.execute('get_live_categories')
    }

    getVODStreamCategories() {
        return this.execute('get_vod_categories')
    }

    getSeriesStreamCategories() {
        return this.execute('get_series')
    }

    /**
     * @param {string} [category]
     */
    async getLiveStreams(category) {
        return await this.execute('get_live_streams', { category_id: category })
    }

    /**
     * @param {string} [category]
     */
    async getVODStreams(category) {
        return await this.execute('get_vod_streams', { category_id: category })
    }

    /**
     * GET VOD Info
     *
     * @param {number} id This will get info such as video codecs, duration, description, directors for 1 VOD
     */

    async getVODInfo(id) {
        if (!id) {
            throw new Error('Vod Id not defined')
        }

        return this.execute('get_vod_info', { vod_id: id })
            .then(res => {
                if (res.hasOwnProperty('info') && res.info.length === 0) {
                    throw new Error(`vod with id: ${id} not found`)
                }
                return res
            })
    }

    /**
    * GET Serie Info
    *
    * @param {number} id This will get info such as video codecs, duration, description, directors for 1 VOD
    */

    async getSerieInfo(id) {
        if (!id) {
            throw new Error('Series Id not defined')
        }

        return this.execute('get_series_info', { series_id: id })
            .then(res => {
                if (res.hasOwnProperty('info') && res.info.length === 0) {
                    throw new Error(`serie with id: ${id} not found`)
                }
                return res
            })
    }

    /**
     * GET short_epg for LIVE Streams (same as stalker portal, prints the next X EPG that will play soon)
     *
     * @param {number} id
     * @param {number} limit You can specify a limit too, without limit the default is 4 epg listings
     */
    async getEPGLiveStreams(id, limit) {
        return await this.execute('get_short_epg', { stream_id: id, limit })
    }

    /**
     * GET ALL EPG for LIVE Streams (same as stalker portal, but it will print all epg listings regardless of the day)
     *
     * @param {number} id
     */
    async getAllEPGLiveStreams(id) {
        return await this.execute('get_simple_data_table', { stream_id: id })
    }


}

export default ApiXtreamCodes;