import moment from 'moment';

export function getSourceURL(stream_type, stream_id, format = 'm3u8') {
	const { server_protocol, url, port } = JSON.parse(localStorage.getItem(`server`));
	const { username, password } = JSON.parse(localStorage.getItem(`user`));
	const resource = `${server_protocol}://${url}:${port}/${stream_type}/${username}/${password}/${stream_id}.${format}`;
	return resource;
}

export function getCatchUpSourceURL(startDate, endDate, stream_id, format = 'm3u8') {
	const { server_protocol, url, port } = JSON.parse(localStorage.getItem(`server`));
	const { username, password } = JSON.parse(localStorage.getItem(`user`));
	const start = moment(startDate).format('YYYY-M-D:hh-mm');
	const dt1 = moment(startDate);
	const dt2 = moment(endDate);
	const duration = dt2.diff(dt1, 'minutes');
	const resource = `${server_protocol}://${url}:${port}/timeshift/${username}/${password}/${duration}/${start}/${stream_id}.${format}`;
	return resource;
}


export function getStartEndTime(date) {
	const d = moment(date);
	const time = d.format("H:mm");
	return time;
}

export function getTime(time) {
	const humanTime = moment.unix(time).format("HH:mm");
	return humanTime;
}

export function getExpDate(date) {
	//const d = new Date(date);
	if (date == null) return 'unlimited';
	const date_format = moment.unix(date).format("MM/DD/YYYY");
	return date_format;
}

export function getFullYear(date) {
	const d = moment(date);
	const time = `${d.year()}`;
	return time;
}

export function getNow() {
	return moment().format();
}

export function genDates(data, tvArchive = 0) {

	if (data.length === 0) return [];

	const group = data.reduce(function (current, item) {

		const date = moment(item.start).format('dddd DD/MM');
		const isMoreThanCurrentDate = moment(item.start).isSameOrAfter(new Date(), "day");

		if (isMoreThanCurrentDate && !tvArchive) {
			current[date] = current[date] || [];
			current[date].push(item);
		}
		else if (tvArchive === 1) {
			current[date] = current[date] || [];
			current[date].push(item);
		}

		return current;

	}, Object.create(null));


	const today = moment().format('dddd DD/MM');
	const tomorrow = moment().add('days', 1).format('dddd DD/MM');

	return Object.keys(group).map(function (k) {

		const dates = {
			date: k,
			today: (k === today) ? true : false
		}
		// check if there are duplicates 
		const hashtable = {};
		const data = [];
		for(let item of group[k]){
			if(!hashtable[item.start]){
				hashtable[item.start] = item.start;
				data.push(item);
			}
		}
		dates.data = data;
		
		if (k === today) dates.date = 'Today';
		if (k === tomorrow) dates.date = 'Tomorrow';

		return dates;
	});

}

export function getDuration(secs) {
	const duration = moment.utc(secs * 1000).format('H:mm:ss');
	return duration;
}


export function storageAvailable(type, items, key) {
	try {
		var storage = window[type],
			x = key,
			y = items;
		storage.setItem(x, y);
		storage.removeItem(x);
		return true;
	}
	catch (e) {
		return e instanceof DOMException && (
			// everything except Firefox
			e.code === 22 ||
			// Firefox
			e.code === 1014 ||
			// test name field too, because code might not be present
			// everything except Firefox
			e.name === 'QuotaExceededError' ||
			// Firefox
			e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
			// acknowledge QuotaExceededError only if there's something already stored
			storage.length !== 0;
	}
}


export function hasM3U8(allowed_output_formats) {
	return allowed_output_formats.includes('m3u8');
}


export function getProgress(start_timestamp, stop_timestamp) {

	let now = Math.round((new Date()).getTime() / 1000);
	let totalTime = stop_timestamp - start_timestamp;
	let progress = now - start_timestamp;
	let percentage = Math.floor((progress / totalTime) * 100);

	return percentage;
}


export function chunk(array, size) {
	const chunked_arr = [];
	let index = 0;
	while (index < array.length) {
		chunked_arr.push(array.slice(index, size + index));
		index += size;
	}
	return chunked_arr;
}
