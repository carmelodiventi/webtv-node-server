export function user() {
    let user = JSON.parse(localStorage.getItem('user'));
    if (user && user.status === 'Active') {
        return user;
    } else {
        return {};
    }
}

export function getFeaturedCategory() {
    let category = JSON.parse(localStorage.getItem('featuredCategory'));
    if (category) {
        const { id } = category;
        return id;
    } else {
        return null;
    }
}