import React from 'react';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { Switch, Route, withRouter } from "react-router-dom";
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import PrivateRoute from './component/PrivateRoute';
import Login from './component/Login';
import SingleOn from './component/SingleOn';
import Featured from './containers/Featured';
import Live from './containers/Live';
import LiveCategory from './containers/live/LiveCategory';
import Series from './containers/Series';
import SerieInfo from './containers/series/SerieInfo';
import Movies from './containers/Movies';
import MoviesCategory from './containers/movies/MoviesCategory';
import MovieInfo from './containers/movies/MovieInfo';
import Account from './containers/Account';

const App = ({ authentication, history: { location } }) => {

  return (
    <TransitionGroup className="page-content">
      <CSSTransition
        timeout={300}
        classNames='page-transition'
        key={location.key}
      >
        <Switch location={location}>
          <Route path="/single-on/:username/:password" match component={SingleOn} />
          <Route path="/login" match component={Login} />
          <PrivateRoute path="/" match exact component={Featured} auth={authentication} />
          <PrivateRoute path="/live" match exact component={Live} auth={authentication} />
          <PrivateRoute path="/live/category/:category_id/" match exact component={LiveCategory} auth={authentication} />
          <PrivateRoute path="/movies" match exact component={Movies} auth={authentication} />
          <PrivateRoute path="/movies/category/:category_id/" match exact component={MoviesCategory} auth={authentication} />
          <PrivateRoute path="/movies/:stream_id/" match exact component={MovieInfo} auth={authentication} />
          <PrivateRoute path="/series" match exact component={Series} auth={authentication} />
          <PrivateRoute path="/series/category/:series_id/" match exact component={SerieInfo} auth={authentication} />
          <PrivateRoute path="/account" match exact component={Account} auth={authentication} />
        </Switch>
      </CSSTransition>
    </TransitionGroup>
  );

}

function mapStateToProps(state) {
  return {
    authentication: state.authentication,
  }
}

export default compose(
  withRouter,
  connect(mapStateToProps, null)
)(App);