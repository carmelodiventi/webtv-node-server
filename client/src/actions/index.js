import * as types from './actionTypes';
import ApiXtreamCodes from '../lib/xtream-codes';


export const login = ({ username, password }) => async dispatch => {

  const api = new ApiXtreamCodes({ baseUrl: process.env.REACT_APP_API_URL, auth: { username, password } })

  try {
    dispatch({ type: types.AUTH_REQUEST });
    const res = await api.getAccountInfo()
    dispatch({ type: types.USER_LOG_IN_REQUEST, payload: res });
    return res;

  } catch (error) {
    dispatch({ type: types.USER_LOG_IN_FAIL, payload: { error: error } });
    return { error: error };
  }

};

export const logout = () => {

  return {
    type: types.USER_LOG_OUT
  }

}

export const getFeatured = () => async dispatch => {


  try {

    if (!localStorage.getItem('featuredCategory')) {
      throw new Error('Featured Category not Found')
    }
    const { id } = JSON.parse(localStorage.getItem('featuredCategory'));
    const { username, password } = JSON.parse(localStorage.getItem('user'));
    const api = new ApiXtreamCodes({ baseUrl: process.env.REACT_APP_API_URL, auth: { username, password } })

    dispatch({ type: types.REQUEST_FEATURED });
    const res = await api.getVODStreams(id)
    dispatch({ type: types.GET_FEATURED, payload: res });
    return res;
    
  } catch (error) {
    dispatch({ type: types.GET_FEATURED_FAIL, payload: { error: error } });
    return { error: error };
  }

}

export const getFeaturedStreamCategories = () => async dispatch => {

  try {
    const { username, password } = JSON.parse(localStorage.getItem('user'));
    const api = new ApiXtreamCodes({ baseUrl: process.env.REACT_APP_API_URL, auth: { username, password } })

    dispatch({ type: types.REQUEST_FEATURED })
    const res = await api.getVODStreamCategories()
    dispatch({ type: types.GET_FEATURED_STREAMS_CATEGORIES, payload: res })
    return res;
    
  } catch (error) {
    dispatch({ type: types.GET_FEATURED_STREAMS_CATEGORIES_FAIL, payload: { error: error } });
    return { error: error };
  }

}


export const getAccountInfo = () => async dispatch => {

  try {
    const { username, password } = JSON.parse(localStorage.getItem('user'));
    const api = new ApiXtreamCodes({ baseUrl: process.env.REACT_APP_API_URL, auth: { username, password } })

    dispatch({ type: types.AUTH_REQUEST });
    const res = await api.getAccountInfo()
    dispatch({ type: types.GET_USER_INFO, payload: res });
    return res;
    
  } catch (error) {
    dispatch({ type: types.GET_USER_INFO_FAIL, payload: { error: error } });
    return { error: error };
  }

};


export const getLiveStreamCategories = () => async dispatch => {

  try {

    const { username, password } = JSON.parse(localStorage.getItem('user'));
    const api = new ApiXtreamCodes({ baseUrl: process.env.REACT_APP_API_URL, auth: { username, password } })

    dispatch({ type: types.REQUEST_LIVE })
    const res = await api.getLiveStreamCategories()
    dispatch({ type: types.GET_LIVE_STREAMS_CATEGORIES, payload: res })
    return res;
    
  } catch (error) {
    dispatch({ type: types.GET_LIVE_STREAMS_CATEGORIES_FAIL, payload: { error: error } });
    return { error: error };
  }

}

export const getLiveStreams = (category_id) => async dispatch => {

  try {

    const { username, password } = JSON.parse(localStorage.getItem('user'));
    const api = new ApiXtreamCodes({ baseUrl: process.env.REACT_APP_API_URL, auth: { username, password } })

    dispatch({ type: types.REQUEST_LIVE })
    const res = await api.getLiveStreams(category_id)
    dispatch({ type: types.GET_LIVE_STREAMS, payload: res })
    return res;
    
  } catch (error) {
    dispatch({ type: types.GET_LIVE_STREAMS_FAIL, payload: { error: error } });
    return { error: error };
  }

}

export const getVODStreamCategories = () => async dispatch => {

  try {
    const { username, password } = JSON.parse(localStorage.getItem('user'));
    const api = new ApiXtreamCodes({ baseUrl: process.env.REACT_APP_API_URL, auth: { username, password } })

    dispatch({ type: types.REQUEST_VOD })
    const res = await api.getVODStreamCategories()
    dispatch({ type: types.GET_VOD_STREAMS_CATEGORIES, payload: res })
    return res;

  } catch (error) {
    dispatch({ type: types.GET_VOD_STREAMS_CATEGORIES_FAIL, payload: { error: error } });
    return { error: error };
  }

}

export const getVODStreams = category_id => async dispatch => {

  try {
    const { username, password } = JSON.parse(localStorage.getItem('user'));
    const api = new ApiXtreamCodes({ baseUrl: process.env.REACT_APP_API_URL, auth: { username, password } })

    dispatch({ type: types.REQUEST_VOD })
    const res = await api.getVODStreams(category_id)
    dispatch({ type: types.GET_VOD_STREAMS, payload: res })
    return res;

  } catch (error) {
    dispatch({ type: types.GET_VOD_STREAMS, payload: { error: error } });
    return { error: error };
  }

}

export const getVODInfo = stream_id => async dispatch => {

  try {
    const { username, password } = JSON.parse(localStorage.getItem('user'));
    const api = new ApiXtreamCodes({ baseUrl: process.env.REACT_APP_API_URL, auth: { username, password } })

    dispatch({ type: types.REQUEST_VOD })
    const res = await api.getVODInfo(stream_id)
    dispatch({ type: types.GET_VOD_INFO_BY_ID, payload: res })
    return res;

  } catch (error) {
    dispatch({ type: types.GET_VOD_INFO_BY_ID_FAIL, payload: { error: error } });
    return { error: error };
  }

}

export const closeVODInfo = () => {
  return {
    type: types.CLOSE_VOD_INFO,
    payload: false
  }
}

export const getSeriesStreamCategories = () => async dispatch => {

  try {
    const { username, password } = JSON.parse(localStorage.getItem('user'));
    const api = new ApiXtreamCodes({ baseUrl: process.env.REACT_APP_API_URL, auth: { username, password } })

    dispatch({ type: types.REQUEST_SERIES })
    const res = await api.getSeriesStreamCategories()
    dispatch({ type: types.GET_SERIES_STREAMS_CATEGORIES, payload: res })
    return res;
  } catch (error) {
    dispatch({ type: types.GET_SERIES_STREAMS_CATEGORIES_FAIL, payload: { error: error } });
    return { error: error };
  }

}

export const getSeriesInfo = serie_id => async dispatch => {

  try {
    const { username, password } = JSON.parse(localStorage.getItem('user'));
    const api = new ApiXtreamCodes({ baseUrl: process.env.REACT_APP_API_URL, auth: { username, password } })

    dispatch({ type: types.REQUEST_SERIES })
    const res = await api.getSerieInfo(serie_id)
    dispatch({ type: types.GET_SERIE_INFO, payload: res })
    return res;
  } catch (error) {
    dispatch({ type: types.GET_SERIE_INFO_FAIL, payload: { error: error } });
    return { error: error };
  }

}

export const closeSerieInfo = () => {
  return {
    type: types.CLOSE_SERIE_INFO,
    payload: false
  }
}