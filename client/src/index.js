import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { ToastProvider } from 'react-toast-notifications';
import thunk from 'redux-thunk';
import reducers from './reducers';
import { BrowserRouter as Router } from "react-router-dom";
import registerServiceWorker from './registerServiceWorker';
import App from './App';
import Header from './component/Header';
import ErrorBoundary from './ErrorBoundary';

import './App.scss';


const store = createStore(
	reducers,
	applyMiddleware(thunk)
);

ReactDOM.render(
	<Provider store={store}>
		<ToastProvider>
			<Router>
				<div className="app">
					<ErrorBoundary>
						<Header />
						<App />
					</ErrorBoundary>
				</div>
			</Router>
		</ToastProvider>
	</Provider>,
	document.getElementById('root')
);

registerServiceWorker();