const _h = require('../helpers/ffmpeg.helper');

const getThumbnail = async (req, res) => {
    try {
        const { url } = req.query;
        const outputPath = __dirname;
        const thumb = await _h.createFragmentPreview(url, outputPath, 4);
        res.status(200).send({ success: true, thumb })
    } catch (error) {
        res.status(404).send({ error: error })
    }

}

module.exports = {
    getThumbnail
}