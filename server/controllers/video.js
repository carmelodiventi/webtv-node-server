const ffmpeg = require('ffmpeg');

const convertVideo = async (req,res) => {

    try {
        // const { video } = req.body || "http://selling-team.net:80/series/carmelodemo/Triatec06/218687.mkv";
        const videoPath = "http://selling-team.net:80/series/carmelodemo/Triatec06/218687.mkv";
        const convertedVideo = await new ffmpeg(videoPath, function (err, video) {
            if (!err) {
                console.log('The video is ready to be processed');
            } else {
                console.log('Error: ' + err);
            }
        });
        //const convertedVideo = await ffmpeg(videoPath).format('mp4').output('/');
        return res.status(200).send({ success: true, convertedVideo});
    } catch (error) {
        return res.status(500).send({success: false, error})
    }

    
}

module.exports = {
    convertVideo
}