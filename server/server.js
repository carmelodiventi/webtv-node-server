const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const logger = require('morgan');
require("dotenv").config();
const cors = require('cors');
const routes = require('./routes');
const port = process.env.NODE_ENV === 'production' ? (process.env.PORT || 80) : 3001;

app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(cors());
app.use('/api/v1', routes);

if (process.env.NODE_ENV !== 'production') {
    app.use(logger('dev'))
}

app.listen(port, () => {
    console.log(`Server is listening at port ${port}`)
})