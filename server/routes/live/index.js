const router = require('express').Router();
const { getThumbnail } = require('../../controllers/live')

router.post('/live', getThumbnail)

module.exports = router;