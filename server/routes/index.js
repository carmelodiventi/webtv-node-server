const routes = require('express').Router();
const live = require('../routes/live');
const video = require('../routes/video');

routes.get('/', (req, res) => {
    res.status(200).json({ message: 'Connected!' });
});

routes.use('/live', live)
routes.use('/video', video)

module.exports = routes;