const router = require('express').Router();
const videoController = require('../../controllers/video');

router.get('/', videoController.convertVideo)

module.exports = router;